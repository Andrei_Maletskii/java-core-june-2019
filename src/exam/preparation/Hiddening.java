package exam.preparation;

public class Hiddening {
	public static void main(String[] args) {
		A.method();
		B.method();

		new A().method();
		new B().method();

		A a = new B();
		a.method();
		B b = null;
		b.method();
	}
}

class A {

	static void method() {
		System.out.println("static A");
	}
}

class B extends A {
	static void method() {
		System.out.println("static B");
	}
}
