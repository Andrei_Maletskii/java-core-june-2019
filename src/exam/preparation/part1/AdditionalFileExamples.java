package exam.preparation.part1;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.UUID;

public class AdditionalFileExamples {

	public static void main(String[] args) {
		File directory =
			new File("C:\\Users\\Andrei_Maletskii\\IdeaProjects\\UUIDTest\\dir4\\dir5\\dir6");

		boolean mkdir = directory.mkdir();
		System.out.println(mkdir);

		boolean mkdirs = directory.mkdirs();
		System.out.println(mkdirs);

		File uuidtest =
			new File("C:\\Users\\Andrei_Maletskii\\IdeaProjects\\UUIDTest");

//		for (String fileName : uuidtest.list()) {
//			System.out.println(fileName);
//		}

		File[] files = uuidtest.listFiles();

		for (File file : files) {
			try {
				System.out.println(file.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println(uuidtest.canRead());
		System.out.println(uuidtest.canWrite());
		System.out.println(uuidtest.canExecute());

		File idea64 =
			new File("C:\\Program Files\\JetBrains\\IntelliJ IDEA 2019.1.3\\bin\\idea64.exe");

		System.out.println(idea64.canExecute());

		File resetTriangle =
			new File("C:\\Users\\Andrei_Maletskii\\IdeaProjects\\UUIDTest\\resetTriangle");

		System.out.println(resetTriangle.canExecute());
		System.out.println(resetTriangle.canWrite());
		System.out.println(resetTriangle.canRead());

		System.out.println("-----");
		File dir7 = new File("./dir7");
		System.out.println("Exists: " + dir7.exists());

		System.out.println("MkDir: " + dir7.mkdir());
		System.out.println("isDirectory: " + dir7.isDirectory());
		System.out.println("isFile: " + dir7.isFile());

		String[] list = dir7.list((dir, name) -> {
			System.out.println(dir);
			System.out.println(name);

			return name.startsWith("output");
		});

		System.out.println("-----");

		for (String s : list) {
			System.out.println(s);
		}

		System.out.println("-----");

		File[] files1 = dir7.listFiles(file -> {
			System.out.println(file);
			return true;
		});

		for (File file : files1) {
			System.out.println(file);
		}


	}
}
