package exam.preparation.part1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Cereal {

	private String name = "CocoaCookies";
	private transient int sugar;
	public Cereal() {
		super();
		this.name = "CaptainPebbles";
	}

	{
		name = "SugarPops";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSugar() {
		return sugar;
	}

	public void setSugar(int sugar) {
		this.sugar = sugar;
	}

	public static void main(String[] args) {
		Cereal cereal = new Cereal();
		cereal.setName("CornLoops");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
			oos.writeObject(cereal);
		} catch (IOException e) {
			e.printStackTrace();
		}

		byte[] cerealArray = baos.toByteArray();

		try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(cerealArray))){
			Object o = ois.readObject();

			Cereal readCereal = (Cereal) o;

			System.out.println(readCereal.getName());
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

	}
}
