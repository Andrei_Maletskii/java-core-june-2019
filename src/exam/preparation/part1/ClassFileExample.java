package exam.preparation.part1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ClassFileExample {
	public static void main(String[] args) {
		File file = new File("C:\\Users\\Andrei_Maletskii\\IdeaProjects\\UUIDTest\\file 1.txt");
		File file1 = new File("C:\\Users\\Andrei_Maletskii\\IdeaProjects\\UUIDTest\\file 1.txt");
		File file2 = new File(".." + File.separator + "UUIDTest");

		System.out.println(file.getPath());

		System.out.println(file.getAbsolutePath());
		try {
			System.out.println(file.getCanonicalPath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		String name = file.getName();
		System.out.println(name);


		String parent = file.getParent();
		File parentFile = file.getParentFile();

		System.out.println("---");

		System.out.println(parent);
		File parentFile1 = parentFile.getParentFile();
		System.out.println(parentFile1);
		System.out.println(parentFile1.getParentFile());


	}
}
