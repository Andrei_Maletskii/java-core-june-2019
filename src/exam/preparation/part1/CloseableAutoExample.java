package exam.preparation.part1;

import java.io.Closeable;
import java.io.IOException;

public class CloseableAutoExample {
	public static void main(String[] args) throws IOException {

		try (AutoCloseableImpl aci = new AutoCloseableImpl();
			CloseableImpl ci = new CloseableImpl();) {

		} catch (Exception e) {
			e.printStackTrace();
		}

		Closeable a = new Ab();
		a.close();
	}
}

class Ab implements Closeable, AutoCloseable {

	@Override
	public void close() {
		System.out.println("hello!");
	}
}

class CloseableImpl implements Closeable {

	@Override
	public void close() throws IOException {
		System.out.println("Closeable closed");
	}
}

class AutoCloseableImpl implements AutoCloseable {

	@Override
	public void close() throws Exception {
		System.out.println("AutoCloseable closed");
	}
}