package exam.preparation.part1;

import java.io.File;
import java.io.IOException;

public class FileExamples {
	public static void main(String[] args) throws IOException {
		File file = new File("../tinypounder");

		System.out.println(file.getPath());
		System.out.println(file.getAbsolutePath());
		System.out.println(file.getCanonicalPath());
	}
}
