package exam.preparation.part1;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;

public class FileSettersExample {


	public static void main(String[] args) {
		File directory = new File("./dir7");

		Optional<File> any = Optional.ofNullable(directory.listFiles()).stream().flatMap(Arrays::stream).findFirst();

		File file = any.orElseThrow(RuntimeException::new);

		System.out.println("Initial: ");
		printMode(file);

		System.out.println("----");
		boolean setExecutable = file.setExecutable(false);
		boolean setWritable = file.setWritable(false);
		boolean setReadable = file.setReadable(false);

		System.out.println("exec set: " + setExecutable);
		System.out.println("write set: " + setWritable);
		System.out.println("read set: " + setReadable);

		printMode(file);

		boolean delete = file.delete();
		System.out.println(delete);

		File file81 = new File("./dir7/outputFileUTF81");

		file81.deleteOnExit();


	}

	private static void printMode(File file) {
		System.out.println(file.canExecute());
		System.out.println(file.canWrite());
		System.out.println(file.canRead());
	}
}
