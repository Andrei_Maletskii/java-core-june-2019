package exam.preparation.part1;

class Wash<T> {

	T item;

	public T getItem() {
		return item;
	}

	public void setItem(T item) {
		this.item = item;
	}
}

class LaundryTime {
	public static void main(String[] args) {
		Wash<String> stringSuperWash = new SuperWash<>();
		String item = stringSuperWash.getItem();

		Wash<LaundryTime> ssw = new UberWash<LaundryTime>();

		assert ssw != null;

		assert true;
	}
}

class SuperWash<T> extends Wash<T> {

}

class UberWash<LaundryTime> extends Wash<LaundryTime> {

	LaundryTime laundryTime;
	exam.preparation.part1.LaundryTime laundryTime1;

}
