package exam.preparation.part1;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

	public static <T> void copyList(List<? extends T> src, List<? super T> dst) {
		for (T t : src) {
			dst.add(t);
		}
	}

	public static void main(String[] args) {
		List<B> blist = new ArrayList<>();

		List<A> list = new ArrayList<>();

		copyList(blist, list);
	}
}

class A {}
class B extends A {}
class C extends B {}
