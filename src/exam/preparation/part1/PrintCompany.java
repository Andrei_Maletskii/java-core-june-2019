package exam.preparation.part1;

import java.io.Closeable;
import java.util.ArrayList;

public class PrintCompany {
	class Printer implements Closeable {
		public void print() {
			System.out.println("This just in!");
		}
		public void close() {}
	}
	public void printHeadlines() {
		try (Printer p = new Printer()) {
			p.print();
		}
	}
	public static void main(String[] args) {
		new PrintCompany().printHeadlines();

		int flakes = 10;

		assert flakes++>0;

		ArrayList ar = new ArrayList<>();
	}
}
