package exam.preparation.part2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class DateTimeFormatterExamples {

	public static void main(String[] args) {
//		DateTimeFormatter dateTimeFormatter = new DateTimeFormatter();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

		String formattedNowRFC = dateTimeFormatter.format(LocalDateTime.now());
		System.out.println(formattedNowRFC);

		System.out.println(LocalDateTime.now());

		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");

		System.out.println(dateFormatter.format(LocalDate.now()));

		System.out.println(DateTimeFormatter.RFC_1123_DATE_TIME.format(ZonedDateTime.now()));

		System.out.println("-----");


		DateTimeFormatter dateTimeFormatterNew =
			DateTimeFormatter.ofPattern("MM-dd-yyyy");

		String format = dateTimeFormatterNew.format(LocalDate.now().plusDays(10));

		System.out.println(format);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh-mm-ss");

		System.out.println(formatter.format(LocalTime.now()));

		DateTimeFormatter dateTimeFormatter1 =
			DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);

		DateTimeFormatter formatter1 = DateTimeFormatter.ISO_DATE;


	}
}
