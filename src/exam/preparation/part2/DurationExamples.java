package exam.preparation.part2;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

public class DurationExamples {

	public static void main(String[] args) {
		Duration zeroDuration = Duration.ZERO;

		Duration duration = Duration.ofDays(1);

		System.out.println(zeroDuration);
		System.out.println(duration);

		Duration between = Duration.between(
			LocalTime.now().plusHours(1),
			LocalTime.now().minusHours(1)
		);

		System.out.println(between);

		Duration negated = between.negated();

		System.out.println(negated);

		LocalTime plus1 = LocalTime.now().plus(Duration.ofHours(1));
		LocalDate plus2 = LocalDate.now().plus(Period.ofDays(1));

		// LocalTime -> Duration
		// LocalDate -> Period

//		LocalTime plus3 = LocalTime.now().plus(Period.ofDays(1)); ERROR
//		LocalDate plus4 = LocalDate.now().plus(Duration.ofHours(1)); ERROR
	}
}
