package exam.preparation.part2;

import java.time.Duration;
import java.time.Instant;

public class InstantExamples {

	public static void main(String[] args) {
		Instant now = Instant.now();

		long timestamp = System.currentTimeMillis();

		System.out.println(now);

		Instant plusTenSeconds = now.plusSeconds(10);

//		System.out.println(plusTenSeconds);

		Instant minus1MNanos = now.minusNanos(1_000_000);

		System.out.println(minus1MNanos);

		long timestamp1 = now.toEpochMilli();

//		System.out.println(timestamp1);

		Duration duration = Duration.between(
			Instant.now().minusNanos(1_000_000),
			Instant.now().plusNanos(1_000_000)
		);

		System.out.println(duration);

		boolean after = Instant.now().isAfter(Instant.MAX);

		Instant epoch = Instant.EPOCH;
		Instant max = Instant.MAX;
		Instant min = Instant.MIN;


	}
}
