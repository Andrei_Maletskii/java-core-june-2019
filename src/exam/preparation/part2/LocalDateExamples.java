package exam.preparation.part2;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZonedDateTime;
import java.time.Period;
import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class LocalDateExamples {

	public static void main(String[] args) {
		LocalDate now = LocalDate.now();

		System.out.println(now);

		LocalDate now2 = LocalDate.of(2019, 9, 5);
		LocalDate now3 = LocalDate.of(2019, Month.SEPTEMBER, 5);

		System.out.println(now2);
		System.out.println(now3);

		LocalDate current = LocalDate.now();

		LocalDate tomorrow = current.plusDays(1);

		System.out.println(tomorrow);
		System.out.println(current);

		LocalDate today = tomorrow.minusDays(1);

		System.out.println(today);

		LocalDate localDate = current.plusMonths(5);

		System.out.println(localDate);

		LocalDate yesterday = localDate.plusDays(-1);

		System.out.println(yesterday);

		LocalDate localDate1 = localDate.minusDays(-1);

		System.out.println(localDate1);

		LocalDate modifiedNow = LocalDate.now().minusDays(-1).plusDays(-1);

		System.out.println(LocalDate.now().equals(modifiedNow));

		int comparisonResult = LocalDate.now().compareTo(LocalDate.now().plusDays(1));
		System.out.println(comparisonResult);

	}

}
