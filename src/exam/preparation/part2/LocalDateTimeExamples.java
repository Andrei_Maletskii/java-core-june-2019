package exam.preparation.part2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

public class LocalDateTimeExamples {

	public static void main(String[] args) {
		ChronoUnit chronoUnit = ChronoUnit.valueOf(args[0]);

		LocalDateTime now = LocalDateTime.now();

		System.out.println(now);

		LocalDateTime now2 = LocalDateTime.of(LocalDate.now(), LocalTime.now());

		now.plusYears(2_000_000);

		LocalDateTime plus = now.plus(2, chronoUnit);

//		now.plus(10, ChronoUnit.ERAS)


	}
}
