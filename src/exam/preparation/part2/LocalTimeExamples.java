package exam.preparation.part2;

import java.time.LocalTime;

public class LocalTimeExamples {

	public static void main(String[] args) {
		LocalTime now = LocalTime.now();

		System.out.println(now);

		LocalTime plusNanos = now.plusNanos(1_000_000);
		LocalTime minusNanos = now.minusNanos(1);

		boolean isAfter = plusNanos.isAfter(LocalTime.now());
		boolean isBefore = LocalTime.now().isBefore(plusNanos);


	}
}
