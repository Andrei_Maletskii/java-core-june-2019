package exam.preparation.part2;

import java.time.LocalDate;
import java.time.Period;

public class PeriodExamples {

	public static void main(String[] args) {
		Period period = Period.ZERO;
//		Period period1 = new Period(); ERROR

		Period period1 = Period.of(1, 2, 3);

		System.out.println(period);
		System.out.println(period1);

		Period period2 = Period.ofYears(1);

		Period twoDays = Period.between(
			LocalDate.now().minusDays(1),
			LocalDate.now().plusDays(1)
		);

		System.out.println(twoDays);

		Period negated = twoDays.negated();
		System.out.println(negated);

		if (negated.isNegative()) {
			Period period3 = negated.negated();
		}

		Period months = Period.ofMonths(24);

		System.out.println(months);

		Period normalizedMonths = months.normalized();

		System.out.println(normalizedMonths);



		Period period3 = Period.ofWeeks(1).ofDays(3);
		Period period4 = Period.ofDays(5);


	}

}
