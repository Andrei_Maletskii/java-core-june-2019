package exam.preparation.part2;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class ZonedDateTimeExample {

	public static void main(String[] args) {
		//		ZonedDateTime now = ZonedDateTime.now();

		//		System.out.println(now);

		ZonedDateTime now2 = ZonedDateTime.of(
			LocalDate.now(),
			LocalTime.now(),
			ZoneId.systemDefault()
		);
		ZonedDateTime now3 = ZonedDateTime.of(
			LocalDate.now(),
			LocalTime.now(),
			ZoneOffset.UTC
		);

		ZonedDateTime nowUtc = ZonedDateTime.now(ZoneOffset.UTC);

		System.out.println(nowUtc);

		System.out.println(now2);
		System.out.println(now3);
	}
}
