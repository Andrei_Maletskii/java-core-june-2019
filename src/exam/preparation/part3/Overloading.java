package exam.preparation.part3;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystemException;

@SuppressWarnings("all")
public class Overloading {

	static void method(Object object) {
		System.out.println("object");
	}

	static void method(FileNotFoundException e) {
		System.out.println("FileNotFoundException");
	}

	static void method(IOException e) {
		System.out.println("IO");
	}

	static void method(FileSystemException e) {
		System.out.println("IO");
	}

	public static void main(String[] args) {
	}
}
