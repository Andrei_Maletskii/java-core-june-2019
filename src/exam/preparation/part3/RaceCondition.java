package exam.preparation.part3;

public class RaceCondition {

	volatile static int i = 0;

	public static void main(String[] args) {
		Runnable runnable = () -> {
			for (int j = 0; j < 1000; j++) {
				int d = i;

				d = d + 1;

				i = d;
				System.out.println(i);
			}
		};
		new Thread(runnable).start();

		new Thread(runnable).start();

		new Thread(runnable).start();

		System.out.println(i);

		synchronized(RaceCondition.class) {

		}
	}

	static synchronized void method() {

	}
}
