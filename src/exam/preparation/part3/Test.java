package exam.preparation.part3;

@SuppressWarnings("all")
public class Test {

	public static void main(String[] args) {
		method(1 + 2);
	}

	static void method(short number) {
		System.out.println("short " + number);
	}

	static void method(int number) {
		System.out.println("int " + number);
	}
}
