package lesson190611;

import java.util.Objects;

class D {
	int i;

	public D(int i) {
		this.i = i;
	}

	@Override public boolean equals(Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }
		D d = (D) o;
		return i == d.i;
	}

	@Override public int hashCode() {
		return Objects.hash(i);
	}
}

public class EqualsAndHashcode {
	public static void main(String[] args) {
		int i = getInt();
		switch (i) {
			case 10:
				System.out.println("10");
				break;
		}

//		String str = switch(i) {
//			case 10 -> "one";
//
//		};
	}

	private static int getInt() {
		return 10;
	}

}
