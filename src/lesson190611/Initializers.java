package lesson190611;

public class Initializers {
	static int i;
	int j;

	static {
		System.out.println("Static Initializer");
		i = 0;
	}

	{
		System.out.println("Initializer");
	}

	public Initializers(int j) {
		System.out.println("Constructor");
		this.j = j;
	}

	public static void main(String[] args) {
		Initializers initializers = new Initializers(1);
		Initializers initializers2 = new Initializers(2);

	}
}
