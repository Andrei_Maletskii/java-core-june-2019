package lesson190611;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Test {
	public static void main(String[] args) throws IOException {
		Set<String> ids = new HashSet<>();
		int duplicates = 0;

		FileWriter fileWriter = new FileWriter("data_for_500000.csv", false);

		fileWriter.write(0 + "," + duplicates + "," + 0 + "\n");

		for (int i = 0; i <= 500000; i++) {
			String shortenedId = UUID.randomUUID().toString().substring(0, 6);

			boolean success = ids.add(shortenedId);

			if (!success) {
				duplicates++;
			}

			if (i % 10000 == 0) {
				if (i != 0) {
					fileWriter.write(i + "," + duplicates + "," + (double)duplicates / i  + "\n");
					fileWriter.flush();
				}
			}
		}

		fileWriter.close();
	}
}
