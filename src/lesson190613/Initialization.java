package lesson190613;

import java.util.Date;

public class Initialization {

	public static void main(String[] args) {
		new InitializeMe();
		System.gc();
	}
}

class InitializeMe {
	final Date date;
	final int k;

	{
		date = new Date();
	}

	public InitializeMe() {
		k = 10;
		date.setTime(10);
	}

	private void doSmth(final int i) {
//		i = 10;

	}

	private void doSmth(final Date date) {
//		date = new Date();
	}
}
