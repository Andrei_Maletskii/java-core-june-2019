package lesson190613;

import java.util.ArrayList;
import java.util.List;

public class Overriding {
	public static void main(String[] args) {
		A a = new B();
		a.sayHello();


	}
}
//if i in(1,2)
class A {
	public void sayHello() {
		System.out.println("A Hello");
	}
}

class B extends A {
	@Override
	public void sayHello() {
		System.out.println("B Hello");
	}
}
