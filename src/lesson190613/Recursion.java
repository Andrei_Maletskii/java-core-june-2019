package lesson190613;

import java.util.List;

public class Recursion {

	public static void main(String[] args) {
		for (int i = 1; i <= 10; i++) {
			System.out.println(i);
		}

		List.of(1, 2, 3, 4, 5).stream().forEach(System.out::println);

		printFrom1ToI(10);
	}

	private static void printFrom1ToI(int i) {
		if (i < 1) {
			return;
		}
		System.out.println(i);
		printFrom1ToI(i - 1);
	}


}
