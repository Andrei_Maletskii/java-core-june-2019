package lesson190613;

public class StaticMethod {
	public StaticMethod() {
		this.m();
	}

	public static void main(String[] args) {
		D.doSmth();
		new D().doSmthElse();


		Class<StaticMethod> dClass = StaticMethod.class;

		new StaticMethod().m();
	}

	public void m() {

	}

}

class D {
	static void doSmth() {
		System.out.println("Done!");
	}

	void doSmthElse() {
		System.out.println("Done smth else!");
	}
}
