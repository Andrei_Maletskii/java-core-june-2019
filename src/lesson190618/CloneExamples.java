package lesson190618;

public class CloneExamples {
	public static void main(String[] args) throws CloneNotSupportedException {
		D d = new D(5);

		D dClone = (D) d.clone();

		System.out.println(d.i == dClone.i);
	}
}

class D {
	int i;

	public D(int i) {
		this.i = i;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
