package lesson190618;

public class Inheritance {
	public static void main(String[] args) {
		B b = new B("hello");
	}
}

class A {
	public A(int i) {
	}

	void doSmth() {
	}

	void doSmth1() {

	}
}

class B extends A {
	public B(String str) {
		super(1);
		System.out.println();
	}

	@Override
	void doSmth() {

	}
}

class C extends B {
	public C() {
		super("hello");
		this.doSmth1();
	}

	static int method1(int i) {
		return i;
	}

	static char method1(char c) {
		return c;
	}

//	static char method1(int i) {
//		return 'c';
//	} ERROR
}
