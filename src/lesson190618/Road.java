package lesson190618;

public class Road {
	public static void main(String[] args) {

	}
}

interface Drivable {
	void drive();

	public static interface Washable {
		public abstract void wash();
	}
}

class Car implements Drivable, Drivable.Washable {

	@Override
	public void drive() {

	}

	@Override
	public void wash() {

	}
}
