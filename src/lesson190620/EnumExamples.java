package lesson190620;

import java.util.ArrayList;

public class EnumExamples {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Seasons currentSeason = Seasons.FALL;

		String fall = Seasons.FALL.toString();

		Seasons seasons = Seasons.valueOf("Hello");

		Seasons winter = Seasons.WINTER;
		ArrayList<String> strs = new ArrayList<>() {
			{
				add("str");
			}
		};

	}
}

enum Seasons {
	WINTER(100) {
		public Seasons getOpposite() {
			return SPRING;
		}
	},
	SPRING(101) {
		public boolean isCold() {
			return true;
		}
	},
	SUMMER(101) {
		public boolean isCold() {
			return true;
		}
	},
	FALL(101) {
		public boolean isCold() {
			return true;
		}
	};

	int days;

	Seasons(int days) {
		this.days = days;
	}

	public void method() {
		System.out.println(this.name());
	}

	public boolean myEquals(Seasons season) {
		return this == season;
	}
}
