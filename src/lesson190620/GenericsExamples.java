package lesson190620;

import java.util.ArrayList;
import java.util.List;

class MedicalStaff {}
class Doctor extends MedicalStaff {}
class HeadDoctor extends Doctor {}
class Nurse extends MedicalStaff {}


public class GenericsExamples<T extends Number> {



	public static void main(String[] args) {
//		List<? super Doctor> list7 =
//				new ArrayList<HeadDoctor>(); // error
		List<? super Doctor> list6 =
				new ArrayList<Doctor>();
		List<? super Doctor> list5 =
				new ArrayList<MedicalStaff>();
		List<? super Doctor> list4 =
				new ArrayList<Object>();

//		list5.add(new Object()); // error
//		list5.add(new MedicalStaff()); // error
		list5.add(new Doctor());
		list5.add(new HeadDoctor());

	}

}