package lesson190620;

import java.util.ArrayList;

public class GenericsExamples2<T extends Number> {

	T value;

	public void setValue(T value) {
		this.value = value;
	}

	public static void main(String[] args) {
		GenericsExamples2 ge1 = new GenericsExamples2();



		GenericsExamples2<Double> ge2 = new GenericsExamples2<>();
		GenericsExamples2<Number> ge3 = new GenericsExamples2<>();

	}
}
