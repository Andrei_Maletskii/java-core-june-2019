package lesson190620;

import lesson190625.TreeSet;

import java.util.ArrayList;
import java.util.List;

public class Mark<T> {
	public T mark;
	public Mark (T value) {
		mark = value;
	}
	public T getMark () {
		return mark;
	}
	public int roundMark () {
		return mark.hashCode();
	}
	/* вместо */ // public boolean sameAny (Mark<T> ob) {
	public boolean sameAny (Mark<?> ob) {
		return roundMark () == ob.roundMark ();
	}
	public boolean same (Mark<T> ob) {
		return getMark () == ob.getMark ();
	}

	public static void main(String[] args) {
		Mark<Integer> mark1 = new Mark<>(1);
		Mark<Integer> mark2 = new Mark<>(2);
		Mark<Double> mark3 = new Mark<>(2.0);

		Mark<List> listMark = new Mark<>(new ArrayList());
		mark1.sameAny(listMark);
		mark1.same(mark2);
		mark1.same(mark1);

	}
}
