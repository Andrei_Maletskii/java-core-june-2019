package lesson190620;

import java.util.Scanner;

public class ScannerExample {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int i = scanner.nextInt();
		String s = scanner.nextLine();

		System.out.println(i);
		System.out.println(s);
	}
}
