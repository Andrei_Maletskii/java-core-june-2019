package lesson190625;

public class AnonymousClasses {


	public static void main(String[] args) throws CloneNotSupportedException {
		A a = new A() {
			@Override
			public void method() {
				System.out.println("I'm anonymous");
			}

			@Override
			protected Object clone() throws CloneNotSupportedException {
				return super.clone();
			}
		};

		a.method();

		A a1 = (A) a.clone();
		a1.method();

	}
}

class A implements Cloneable{
	public void method() {
		System.out.println("I'm A class");
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
