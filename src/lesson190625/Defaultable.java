package lesson190625;

public class Defaultable {
	private MyInterface myInterface;
	private MyInterface2 myInterface2;

	public Defaultable(MyInterface myInterface, MyInterface2 myInterface2) {
		this.myInterface = myInterface;
		this.myInterface2 = myInterface2;
	}

	public void method() {
		myInterface.myDefaultMethod();
		myInterface2.myDefaultMethod();
	}

	public static void main(String[] args) {
	}
}

interface MyInterface {
	default void myDefaultMethod() {
		System.out.println("default");
	}
}

interface MyInterface2 {
	default void myDefaultMethod() {
		System.out.println("default2");
	}
}
