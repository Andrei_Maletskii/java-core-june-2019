package lesson190625;

import java.io.IOException;

public class ExceptionExamples {

	static int devide(int value) {
		return 5/value;
	}

	static void method() {
		try {
			throw new IOException();
		} catch (IOException e) {

		}
	}

	public static void main(String[] args) {
		try {
			devide(0);
		} catch (ArithmeticException e) {
			System.out.println("Devision by zero!");
		}

//		try {
		method();
//		} catch (ArithmeticException e) {
//			System.out.println("ArithmeticException");
//		}
	}
}
