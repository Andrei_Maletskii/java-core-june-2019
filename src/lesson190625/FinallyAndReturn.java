package lesson190625;

public class FinallyAndReturn {
	private static int age = 20;

	public static int getAgeWoman() {
		try {
			return age - 3;
		} finally {
			return age;
		}
	}

	public static void main(String[] args) {
		System.out.println(getAgeWoman());
	}
}

