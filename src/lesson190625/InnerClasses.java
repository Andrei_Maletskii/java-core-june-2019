package lesson190625;

import static lesson190625.Outer1.StaticNested.staticM;

public class InnerClasses {
	public static void main(String[] args) {
		Outer outer = new Outer();
		Outer.Inner inner = outer.new Inner();

		inner.method();
//		Outer.Inner inner = new Inner();
		staticM();

	}
}

class Outer {
	int i = 5;
	static int x = 0;

	class Inner {

		int i = 10;
//		static int staticInt = 0;
		final static int staticInt1 = 0;

		void method() {
			System.out.println(this.i);
			System.out.println(Outer.this.i);
		}
	}

	Executable method() {
		class Local implements Executable {
			@Override
			public void execute() {
				System.out.println("i'm local");
			}
		}

		return new Local();
	}
}

interface Executable {
	void execute();
}
