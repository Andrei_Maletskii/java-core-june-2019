package lesson190625;

public class OwnExceptions {

	public static void main(String[] args) throws MyThrowable {
		throw new MyError("hello");
	}
}

class MyError extends Error {
	public MyError(String message) {
		super(message);
	}
}

class MyThrowable extends Throwable {
	public MyThrowable(String message) {
		super(message);
	}
}

class MyException extends Exception {
	public MyException(String message) {
		super(message);
	}
}

class MyRuntimeException extends RuntimeException {
	public MyRuntimeException(String message) {
		super(message);
	}
}
