package lesson190625;

public class StaticNestedClasses {
	public static void main(String[] args) {
		Outer1.StaticNested staticNested = new Outer1.StaticNested();
		Outer1.StaticNested staticNested1 = new Outer1.StaticNested();

		Outer1.StaticNested.staticM();

		Outer1 outer1 = new Outer1();
//		outer1.StaticNested

		staticNested.method();
	}
}

class Outer1 {
	int x = 5;
	static int y = 10;

	public static class StaticNested {
		int i = 115;
		static int d = 100;

		static void staticM() {
			System.out.println("static method");
		}

		public void method() {
			System.out.println("method of static nested");
		}


	}

}