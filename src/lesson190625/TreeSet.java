package lesson190625;

public class TreeSet<T extends Comparable<T>> {
	private Node root;



	private class Node {
		public static final int i = 4;
		private T value;
		private Node left;
		private Node right;

		Node(T value) {
			this.value = value;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();

			if (left != null) {
				sb.append(left.toString());
			}

			sb.append('[').append(value.toString()).append(']');

			if (right != null) {
				sb.append(right.toString());
			}

			return sb.toString();
		}
	}

	public void put(T element) {
		if (root == null) {
			root = new Node(element);
			return;
		}

		put(root, element);
	}

	private void put(Node node, T element) {
		int compareResult = node.value.compareTo(element);

		if (compareResult > 0) {
			if (node.left == null) {
				node.left = new Node(element);

				return;
			}

			put(node.left, element);
		} else {
			if (node.right == null) {
				node.right = new Node(element);

				return;
			}

			put(node.right, element);
		}
	}

	@Override
	public String toString() {
		return root.toString();
	}

	public static void main(String[] args) {
//		TreeSet<Integer> treeSet = new TreeSet<>();
//
//		treeSet.put(4);
//		treeSet.put(2);
//		treeSet.put(1);
//		treeSet.put(3);
//		treeSet.put(6);
//		treeSet.put(5);
//		treeSet.put(7);
//
//		System.out.println(treeSet.toString());

	}
}
