package lesson190625;

import java.io.InterruptedIOException;
import java.net.SocketException;

public class TryFinally {
	public static void main(String[] args) {
		method();
	}

	static void method() {
		AutoClosableImpl closable = new AutoClosableImpl();
		try {
			closable.method();
		} catch (InterruptedIOException e) {
			throw new RuntimeException(e);
		} catch (RuntimeException e) {

		} catch (SocketException e) {
			handleException(e);
			e.printStackTrace();
		}
	}

	static void handleException(SocketException e) {
			try {
				throw e;
			} catch (SocketException ex) {
				ex.printStackTrace();
			}
	}
}

class AutoClosableImpl implements AutoCloseable {

	@Override
	public void close() throws Exception {
		System.out.println("I'm closing");
	}

	public void method() throws InterruptedIOException, SocketException {
		// logic
	}
}
