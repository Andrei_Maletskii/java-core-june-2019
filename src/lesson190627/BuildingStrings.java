package lesson190627;

import java.util.function.Function;

public class BuildingStrings {
	private static final int N = 10_000_000;

	public static void main(String[] args) {
//		System.out.println(measurePerfomance("abc", BuildingStrings::concat));
		System.out.println(measurePerfomance("tyu", BuildingStrings::slowConcat));
		System.out.println(measurePerfomance("poi", BuildingStrings::fastConcat));
	}

	static String concat(String str) {
		String result = "";
		for (int i = 0; i < N; i++) {
			result += str;
		}
		return result;
	}

	static String fastConcat(String str) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < N; i++) {
			sb.append(str).append(":");
		}
		return sb.toString();
	}

	static String slowConcat(String str) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < N; i++) {
			sb.append(str).append(":");
		}
		return sb.toString();
	}

	static long measurePerfomance(String str, Function<String, String> stringFunction) {
		long start = System.currentTimeMillis();
		String apply = stringFunction.apply(str);
		long stop = System.currentTimeMillis();
//		System.out.println(apply);
		return stop - start;
	}
}


