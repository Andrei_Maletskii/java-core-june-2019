package lesson190627;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringCompare {

	public static void main(String[] args) {

		ArrayList<String> list = new ArrayList<>() {
			{
				add("Aa");
				add("aa");
				add("aA");
				add("Bb");
				add("bb");
				add("bB");
			}
		};

		Collections.sort(list);
		System.out.println(list);


		list.sort(String::compareToIgnoreCase);
		System.out.println(list);
		System.out.println();

	}
}
