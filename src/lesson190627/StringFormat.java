package lesson190627;

import java.time.ZonedDateTime;

public class StringFormat {
	public static void main(String[] args) {
		String value = String.format("%s: %s", "Value", "5");
		System.out.println(value);

		System.out.format("%tc", ZonedDateTime.now());

	}
}
