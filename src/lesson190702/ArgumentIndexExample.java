package lesson190702;

import java.util.Formatter;

public class ArgumentIndexExample {
	public static void main(String[] args) {
		Formatter formatter = new Formatter();

		double d1 = 16.78967;
		double d2 = 205.78967;
		formatter.format("%2$e, %<f, %1$g, %<a\n", d1, d2);
		System.out.println(formatter);

	}
}
