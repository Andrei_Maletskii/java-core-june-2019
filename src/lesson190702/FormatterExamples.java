package lesson190702;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;

public class FormatterExamples {

	public static void main(String[] args) throws FileNotFoundException {
		Formatter formatter = new Formatter("hello.txt");

		for (int i = 0; i < 100; i++) {
			formatter.format("%s: %d%n", "Value", i);
		}

		formatter.close();
	}
}
