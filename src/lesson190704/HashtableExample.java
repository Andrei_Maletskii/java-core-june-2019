package lesson190704;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

public class HashtableExample {
	public static void main(String[] args) {
		Hashtable<String, String> hashtable = new Hashtable<>();

		hashtable.put("1", "One");
		hashtable.put("2", "Two");
		hashtable.put("3", "Three");

		System.out.println("get 1: " + hashtable.get("1"));


		Collection values = hashtable.values();
		Iterator itr = values.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
		System.out.println();
		values.remove("One");
		Enumeration e = hashtable.elements();
		while (e.hasMoreElements()) {
			System.out.println(e.nextElement());
		}
	}
}
