package lesson190704;

import java.util.ArrayList;
import java.util.function.Predicate;

class MyPredicate implements Predicate<String> {

	@Override
	public boolean test(String s) {
		switch (s) {
			case "one":
			case "two":
				return true;
			default:
				return false;
		}
	}
}

public class ToArrayExample {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		ArrayList<Integer> list2 = new ArrayList<>();


		list.add("one");
		list.add("two");
		list.add("three");

		list2.add(1);

		list.retainAll(list2);
		System.out.println("Arraylist<String>: " + list);

		//		Object[] objects = list.toArray();
		//
		//		String[] strings = list.toArray(new String[0]);


//		list.removeIf("one"::equals);
//
//		System.out.println("Arraylist<String>: " + list);

	}
}
