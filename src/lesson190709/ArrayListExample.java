package lesson190709;

import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) {
		int[] array = new int[10];
		Integer[] integerArray = new Integer[10];

		ArrayList<Integer> arrayList = new ArrayList<>();
//		ArrayList<int> intList = new ArrayList<>(); ERROR

		arrayList.add(1);
		arrayList.add(2);
		arrayList.add(3);
		arrayList.add(4);

		arrayList.remove(1);
		arrayList.remove(Integer.valueOf(1));
		Integer i = 1;

		arrayList.remove(i);

		System.out.println(arrayList);
	}
}
