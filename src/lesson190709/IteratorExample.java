package lesson190709;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class IteratorExample {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();

		list.add("one");
		list.add("two");
		list.add("three");

		Iterator<String> iterator = list.iterator();

		while (iterator.hasNext()) {
			String next = iterator.next();
			System.out.println(next);
		}

		Arrays.toString(new int[0]);
	}
}
