package lesson190711;

import java.util.ArrayList;

public class EqualsHashCodeExample {
	int i;
	int j;

	String str;
	ArrayList<String> arrayList;

	public EqualsHashCodeExample(int i, int j, String str, ArrayList<String> arrayList) {
		this.i = i;
		this.j = j;
		this.str = str;
		this.arrayList = arrayList;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		EqualsHashCodeExample that = (EqualsHashCodeExample) o;

		if (i != that.i) { return false; }
		if (j != that.j) { return false; }
		if (str != null ? !str.equals(that.str) : that.str != null) { return false; }
		return arrayList != null ? arrayList.equals(that.arrayList) : that.arrayList == null;
	}

	@Override
	public int hashCode() {
		int result = i;
		result = 31 * result + j;
		result = 31 * result + (str != null ? str.hashCode() : 0);
		result = 31 * result + (arrayList != null ? arrayList.hashCode() : 0);
		return result;
	}
}
