package lesson190711;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;

public class MapExamples {
	private static final int N = 10000000;

	public static void main(String[] args) {
		Map<String, String> randomMap = getRandomMap(N);

		System.out.println("common method estimated: " +
				measurePerfomance(randomMap, MapExamples::iterateMap));
		System.out.println("inlined iterator estimated: " +
				measurePerfomance(randomMap, MapExamples::badIterateMap));
	}

	static String iterateMap(Map<String, String> map) {
		StringBuilder sb = new StringBuilder();

		Set<Map.Entry<String, String>> entries = map.entrySet();

		Iterator<Map.Entry<String, String>> iterator = entries.iterator();

		while(iterator.hasNext()) {
			Map.Entry<String, String> entry = iterator.next();
			sb.append(entry.getKey());
		}

		return sb.toString();
	}

	static String badIterateMap(Map<String, String> map) {
		StringBuilder sb = new StringBuilder();

		Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();

		while(iterator.hasNext()) {
			Map.Entry<String, String> entry = iterator.next();
			sb.append(entry.getKey());
		}

		return sb.toString();
	}

	static long measurePerfomance(
			Map<String, String> map,
			Function<Map<String, String>, String> stringFunction
	) {
		long start = System.currentTimeMillis();
		String result = stringFunction.apply(map);
		long stop = System.currentTimeMillis();
//		System.out.println(result);
		return stop - start;
	}



	static Map<String, String> getRandomMap(int size) {
		Map<String, String> map = new HashMap<>(size);

		Random random = new Random();

		for (int i = 0; i < size; i++) {
			int key = random.nextInt(N);
			int value = random.nextInt(N);

			map.put(Integer.toString(key), Integer.toString(value));
		}

		return map;
	}
}
