package lesson190716;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CollectionExample {
	public static void main(String[] args) {
		List<String> list1
				= Arrays.asList("yellow","red","green","blue");
		List<String> list2 = Arrays.asList("white","black");

		Collections.copy(list1, list2);

		System.out.println(list1);
		System.out.println(list2);


		List<Integer> ints = Arrays.asList(1, 3, 6, 50, 11, 17, 20, 25);

		System.out.println(Collections.binarySearch(ints, 60));


	}
}
