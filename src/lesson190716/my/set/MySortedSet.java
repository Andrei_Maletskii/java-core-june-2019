package lesson190716.my.set;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MySortedSet<E extends Comparable<E>> extends MyAbstractSortedSet<E> {
	Node<E> head;
	int size;

	@Override
	public boolean add(E e) {
		Node<E> prev = null;
		Node<E> next = head;

		while (next != null) {
			int comparisonResult = e.compareTo(next.item);
			if (comparisonResult < 0){
				break;
			}
			if (comparisonResult == 0) {
				return false;
			}
			prev = next;
			next = next.next;
		}

		Node<E> node = new Node<>(e, next);

		if (prev == null) {
			head = node;
		} else {
			prev.next = node;
		}

		size++;
		return true;
	}

	@Override
	public boolean contains(Object o) {
		@SuppressWarnings("unchecked")
			E e = (E) o;

		Node<E> next = head;

		while (next != null) {
			int comparisonResult = e.compareTo(next.item);
			if (comparisonResult < 0){
				return false;
			}
			if (comparisonResult == 0) {
				return true;
			}
			next = next.next;
		}

		return false;
	}

	@Override
	public boolean remove(Object o) {
		@SuppressWarnings("unchecked")
			E e = (E) o;

		Node<E> prev = null;
		Node<E> next = head;

		while (next != null) {
			int comparisonResult = e.compareTo(next.item);
			if (comparisonResult < 0){
				break;
			}
			if (comparisonResult == 0) {
				if (prev == null) {
					head = head.next;
				} else {
					prev.next = next.next;
				}
				size--;
				return true;
			}
			prev = next;
			next = next.next;
		}

		return false;
	}

	@Override
	public void clear() {
		head = null;
		size = 0;
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<>() {
			Node<E> current = head;

			@Override
			public boolean hasNext() {
				return current != null;
			}

			@Override
			public E next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}

				E item = current.item;
				current = current.next;
				return item;
			}
		};
	}

	@Override
	public int size() {
		System.out.println(System.currentTimeMillis());
		return size;
	}

	@Override
	public boolean intersects(MyAbstractSortedSet<E> set) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MyAbstractSortedSet<E> union(MyAbstractSortedSet<E> set) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("[");

		Node<E> current = head;
		while (current != null) {
			sb.append(current.item);

			if (current.next != null) {
				sb.append(',');
			}

			current = current.next;
		}

		return sb.append(']').toString();
	}

	public static void main(String[] args) {
		MySortedSet<Integer> set = new MySortedSet<>();

		set.add(1);
		set.add(2);
		set.add(4);
		set.add(5);

		System.out.println(set.contains(1));
		System.out.println(set.contains(2));
		System.out.println(set.contains(4));
		System.out.println(set.contains(5));
	}
}
