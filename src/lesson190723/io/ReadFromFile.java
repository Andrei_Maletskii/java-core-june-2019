package lesson190723.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.IntSummaryStatistics;

public class ReadFromFile {
	public static void main(String[] args) {
		System.out.println(getIntFileStatistics("scores.txt"));
		System.out.println(getIntFileStatistics("scores2.txt"));
	}

	private static IntSummaryStatistics getIntFileStatistics(String fileName) {
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

			FileInputStream fis = new FileInputStream(fileName);
			byte[] buffer = new byte[10];
			int count = fis.read(buffer);


			BufferedInputStream bis = new BufferedInputStream(fis);


//			for (String buffer = br.readLine(); buffer != null; buffer = br.readLine()) {
//				System.out.println(buffer);
//			}
			//
			//			return br.lines().mapToInt(Integer::parseInt).summaryStatistics();
			return null;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
