package lesson190723.io;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class StreamExamples {

	public static void main(String[] args) throws IOException {
		PipedInputStream pis = new PipedInputStream();
		PipedOutputStream pos = new PipedOutputStream(pis);

		pos.write(10);
		pos.write(12);

		int read = pis.read();

		System.out.println(read);

		int read2 = pis.read();

		System.out.println(read2);
	}
}
