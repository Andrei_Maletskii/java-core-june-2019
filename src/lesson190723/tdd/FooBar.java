package lesson190723.tdd;

// TDD - Test Driven Development

/*
	String fooBar(int i)


	i % 3 == 0 -> foo
	i % 5 == 0 -> bar
	i % 15 == 0 -> foobar
	nothing from above -> nothing

	1) Write test case
	2) Run the test, if it fails -> go 3)
	3) Make the smallest changes to make the test work
 */

public class FooBar {

	public String fooBar(int i) {
		if ((i % 3 == 0) && (i % 5 == 0)) {
			return "foobar";
		}

		if (i % 3 == 0) {
			return "foo";
		}

		if (i % 5 == 0) {
			return "bar";
		}

		return "nothing";
	}
}
