package lesson190725;

import lesson190725.triangle.Triangle;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ObjectInputExamples {
	public static void main(String[] args) {
		try (FileInputStream fis = new FileInputStream("triangle.txt");
				ObjectInputStream ois = new ObjectInputStream(fis)) {
			Object readObject = ois.readObject();

			System.out.println(readObject instanceof Triangle);
			System.out.println(readObject.getClass());
			Triangle triangle = (Triangle) readObject;
			System.out.println(triangle);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
