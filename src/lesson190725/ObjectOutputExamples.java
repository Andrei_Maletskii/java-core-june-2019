package lesson190725;

import lesson190725.triangle.Point;
import lesson190725.triangle.Triangle;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ObjectOutputExamples {
	public static void main(String[] args) {
		Point firstPoint = new Point(0, 0);
		Point secondPoint = new Point(5, 0);
		Point thirdPoint = new Point(0, 5);

		Triangle triangle = new Triangle(firstPoint, secondPoint, thirdPoint);

		try (FileOutputStream fos = new FileOutputStream("triangle.txt");
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			oos.writeObject(triangle);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
