package lesson190725;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.SequenceInputStream;

public class SequenceInput {
	public static void main(String[] args) throws IOException {
		FileInputStream inFile1 = new FileInputStream("file 1.txt");
		FileInputStream inFile2 = new FileInputStream("file 2.txt");
		SequenceInputStream sequenceStream =
			new SequenceInputStream(inFile1, inFile2);
		FileOutputStream outFile = new FileOutputStream("file 3.txt");

		for (
			int readedByte = sequenceStream.read();
			readedByte != -1;
			readedByte = sequenceStream.read()
		) {
			outFile.write(readedByte);
		}

	}
}
