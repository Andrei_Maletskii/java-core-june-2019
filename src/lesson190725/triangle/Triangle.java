package lesson190725.triangle;

public class Triangle extends AbstractTriangle {
	private final Point firstPoint;
	private final Point secondPoint;
	private final Point thirdPoint;

	public Triangle(Point firstPoint, Point secondPoint, Point thirdPoint) {
		this.firstPoint = firstPoint;
		this.secondPoint = secondPoint;
		this.thirdPoint = thirdPoint;
	}

	@Override
	public String toString() {
		return "Triangle{" +
			"firstPoint=" + firstPoint +
			", secondPoint=" + secondPoint +
			", thirdPoint=" + thirdPoint +
			'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }

		Triangle triangle = (Triangle) o;

		if (firstPoint != null ? !firstPoint.equals(triangle.firstPoint) : triangle.firstPoint != null) { return false; }
		if (secondPoint != null ? !secondPoint.equals(triangle.secondPoint) : triangle.secondPoint != null) { return false; }
		return thirdPoint != null ? thirdPoint.equals(triangle.thirdPoint) : triangle.thirdPoint == null;
	}

	@Override
	public int hashCode() {
		int result = firstPoint != null ? firstPoint.hashCode() : 0;
		result = 31 * result + (secondPoint != null ? secondPoint.hashCode() : 0);
		result = 31 * result + (thirdPoint != null ? thirdPoint.hashCode() : 0);
		return result;
	}
}
