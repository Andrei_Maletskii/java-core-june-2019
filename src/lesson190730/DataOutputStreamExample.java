package lesson190730;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class DataOutputStreamExample {

	public static void main(String[] args) throws IOException {
		DataOutputStream dos = new DataOutputStream(new FileOutputStream("numbers"));

		dos.writeLong(5);
		dos.writeInt(5);
		dos.writeShort(5);
		dos.writeByte(5);
		dos.writeInt(10);
		dos.writeInt(15);
		dos.writeInt(20);
		dos.writeInt(30);
		dos.writeInt(50);

		dos.close();

		DataInputStream dis = new DataInputStream(new FileInputStream("numbers"));

		System.out.println(dis.readLong());
		System.out.println(dis.readShort());
		System.out.println(dis.readByte());
		while (dis.available() > 0) {
			System.out.println(dis.readInt());
		}

		dis.close();
	}
}
