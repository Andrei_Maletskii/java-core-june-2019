package lesson190730;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.Consumer;

public class FileReaderExample {

	public static void main(String[] args) throws IOException {
		FileReader fr = new FileReader("src/lesson190730/FileReaderExample.java");

		BufferedReader br = new BufferedReader(fr);

//		String s;
//		while ((s = br.readLine()) != null) {
//			System.out.println(s);
//		}
//		fr.close();

		br.lines().forEach(new Consumer<String>() {
			@Override
			public void accept(String s) {
				System.out.println(s);
			}
		});

		br.close();

	}
}
