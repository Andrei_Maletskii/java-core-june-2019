package lesson190730;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class ScannerExamples {
	public static void main(String[] args) throws FileNotFoundException {
		Scanner scan = new Scanner(System.in);

		while (scan.hasNext()) {
			if (scan.hasNextInt()) {
				System.out.println(scan.nextInt() + ":int");
			} else if (scan.hasNextDouble()) {
				System.out.println(scan.nextDouble() + ":double");
			} else if (scan.hasNextBoolean()) {
				System.out.println(scan.nextBoolean() + ":boolean");
			} else {
//				String next = scan.next();
				String next = scan.nextLine();

				if ("STOP".equals(next)) {
					break;
				}

				System.out.println(next + ":String");
			}
		}
		scan.close();
	}
}
