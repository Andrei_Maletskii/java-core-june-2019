package lesson190730;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class WriteToFileWithEncodingExample {
	public static void main(String[] args) throws IOException {
		printStringToFile("UTF8");
		printStringToFile("UTF16");
		printStringToFile("UTF_16LE");
		printStringToFile("UTF_16BE");
		printStringToFile("UTF32");
		printStringToFile("UTF_32LE");
		printStringToFile("UTF_32BE");
	}

	static void printStringToFile(String encoding) throws FileNotFoundException, UnsupportedEncodingException {
		FileOutputStream fos = new FileOutputStream("outputFile" + encoding);

		OutputStreamWriter osw = new OutputStreamWriter(
			fos,
			encoding
		);
		BufferedWriter bw = new BufferedWriter(
			osw
		);
		PrintWriter pw = new PrintWriter(
			bw
		);

		pw.println("asdf");


		pw.close();
	}

	static void simplePrintStringToFile(String encoding) throws IOException {
		PrintWriter pw2 = new PrintWriter("outputFile" + encoding, encoding);

		pw2.println("asdf2");

		pw2.close();
	}
}
