package lesson190801;

public class ConcurrencyExample {

	public static void main(String[] args) {

		Thread thread1 = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("hello");
			}
		});
		Thread thread2 = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("world");
			}
		});

		thread1.start();
		thread2.start();
	}
}
