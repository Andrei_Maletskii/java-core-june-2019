package lesson190801;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class InheritanceSerialization {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		B toSave = new B(15, 30);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);

		oos.writeObject(toSave);

		byte[] bytes = baos.toByteArray();

		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bytes));

		B savedObj = (B) ois.readObject();

		System.out.println(savedObj);


	}
}

class A {
	int i = 45;

	public A() {
		System.out.println("hello");
	}

	public A(int i) {
		this.i = i;
	}
}

class B extends A implements Serializable {
	int j;

	public B(int i, int j) {
		super(i);
		this.j = j;
	}

	@Override public String toString() {
		return "B{" +
			"i=" + i +
			", j=" + j +
			'}';
	}
}



