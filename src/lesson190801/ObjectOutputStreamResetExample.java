package lesson190801;

import lesson190725.triangle.Point;
import lesson190725.triangle.Triangle;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.function.BiPredicate;

public class ObjectOutputStreamResetExample {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Triangle triangle = new Triangle(
			new Point(0, 1),
			new Point(5, 5),
			new Point(13, 15)
		);

		writeToFileWithoutReset(triangle, "noResetTriangle");
		writeToFileWithReset(triangle, "resetTriangle");

		checkWrittenTriangles(
			triangle,
			"resetTriangle",
			new BiPredicate<Triangle, Triangle>() {
				@Override
				public boolean test(Triangle triangle1, Triangle triangle2) {
					return triangle1 != triangle2;
				}
			}
		);
		checkWrittenTriangles(
			triangle,
			"noResetTriangle",
			new BiPredicate<Triangle, Triangle>() {
				@Override
				public boolean test(Triangle triangle1, Triangle triangle2) {
					return triangle1 == triangle2;
				}
			}
		);
	}

	private static void checkWrittenTriangles(
		Triangle triangle,
		String fileName,
		BiPredicate<Triangle, Triangle> identityCheck
	) throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName));

		Triangle triangle1 = (Triangle) ois.readObject();
		Triangle triangle2 = (Triangle) ois.readObject();

		assert triangle.equals(triangle1);
		assert triangle.equals(triangle2);
		assert triangle1.equals(triangle2);

		assert triangle != triangle1;
		assert triangle != triangle2;
		assert identityCheck.test(triangle1, triangle2);
	}

	private static void writeToFileWithoutReset(Triangle triangle, String fileName) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));

		oos.writeObject(triangle);
		oos.writeObject(triangle);
		oos.close();
	}

	private static void writeToFileWithReset(Triangle triangle, String fileName) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));

		oos.writeObject(triangle);
		oos.reset();
		oos.writeObject(triangle);
		oos.close();
	}


}
