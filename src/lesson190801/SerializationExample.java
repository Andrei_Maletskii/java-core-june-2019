package lesson190801;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationExample {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Object objSave = new Integer(1);
		ObjectOutputStream oos = new ObjectOutputStream(os);
		oos.writeObject(objSave);

		byte[] bArray = os.toByteArray();
		for (byte b : bArray) {
			System.out.print((char) b);
		}
		System.out.println();

		// десериализация
		ByteArrayInputStream is = new ByteArrayInputStream(bArray);
		ObjectInputStream ois = new ObjectInputStream(is);
		Object objRead = ois.readObject();
		Integer objRead1 = (Integer) objRead;
		// проверяем идентичность объектов
		System.out.println("readed object is: "
			+ objRead.toString());
		System.out.println("Object equality is: "
			+ (objSave.equals(objRead)));
		System.out
			.println("Reference equality is: "
				+ (objSave == objRead));

	}
}
