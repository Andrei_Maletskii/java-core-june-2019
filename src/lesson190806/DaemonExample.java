package lesson190806;

import lesson190613.Fibonacci;

public class DaemonExample {

	public static void main(String[] args) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					System.out.println("Profiling...");

					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		thread.setDaemon(true);

		thread.start();

		long result = Fibonacci.fib1(45);
		System.out.println(result);
	}
}
