package lesson190806;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Example {
	private void method() {
		System.out.println("i'm private");
	}


	public static void main(String[] args)
		throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException,
		InstantiationException {
		Class<?> exampleClass = Class.forName("lesson190806.Example");
		Constructor<?> declaredConstructor = exampleClass.getDeclaredConstructor();
		Object exampleObject = declaredConstructor.newInstance();

		Method[] declaredMethods = exampleClass.getDeclaredMethods();

		Method targetMethod = null;
		for (Method declaredMethod : declaredMethods) {
			if (declaredMethod.getName().equals("method")) {
				targetMethod = declaredMethod;
				break;
			}
		}

		targetMethod.setAccessible(true);

		targetMethod.invoke(exampleObject);

	}
}
