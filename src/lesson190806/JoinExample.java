package lesson190806;

import lesson190613.Fibonacci;

public class JoinExample {

	public static void main(String[] args) {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Thread worker = new Thread(new Worker());
		worker.setName("Worker");
		Thread joiner = new Thread(new Joiner(worker));
		joiner.setName("Joiner");
		Thread watcher = new Thread(new Watcher(joiner));
		watcher.setName("Watcher");

		System.out.println(worker.getId());
		System.out.println(joiner.getId());
		System.out.println(watcher.getId());

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		watcher.start();
		worker.start();
		joiner.start();
	}
}

class Worker implements Runnable {
	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			long result = Fibonacci.fib1(100);

			System.out.println(result);
		}
	}
}

class Joiner implements Runnable {
	private Thread threadToJoin;

	public Joiner(Thread threadToJoin) {
		this.threadToJoin = threadToJoin;
	}

	@Override
	public void run() {
		try {
			threadToJoin.join();


		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("I'm glad that you are here!");
	}
}

class Watcher implements Runnable {
	private Thread threadToWatch;

	public Watcher(Thread threadToWatch) {
		this.threadToWatch = threadToWatch;
	}

	@Override
	public void run() {

		for (int i = 0; i < 1000000; i++) {
			System.out.println(threadToWatch.getState());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}


	}
}

