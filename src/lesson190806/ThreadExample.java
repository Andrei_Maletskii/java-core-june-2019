package lesson190806;

public class ThreadExample {

	public static void main(String[] args) {
		Walk walk = new Walk();
		walk.run();
		walk.start();

		Thread talk = new Thread(new Talk());
		talk.start();

		Thread thread1 = new Thread() {
			@Override
			public void run() {
				System.out.println("thread1");
			}
		};

		thread1.start();

		Thread thread2 = new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("thread2");
			}
		});
	}
}

class Walk extends Thread {
	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			System.out.println("Walking...");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class Talk implements Runnable {
	@Override
	public void run() {
		for (int i = 0; i < 100; i++) {
			System.out.println("Talking...");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}