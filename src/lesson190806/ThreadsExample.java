package lesson190806;

public class ThreadsExample {
	public static final int N = 1000000;

	public static void main(String[] args) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < N; i++) {
//					System.out.println(
//						Thread.currentThread().getName() + " is working " + i
//					);
					double pow = Math.pow(10, 10);

					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		System.out.println(thread.getState());

		thread.setName("MyDearThread");

		thread.start();


		for (int i = 0; i < 1000; i++) {
			System.out.println(thread.getState());

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}




	}
}
