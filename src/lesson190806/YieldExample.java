package lesson190806;

public class YieldExample {
	public static void main(String[] args) {
		Thread happyCounter = new Thread(new HappyCounter());
		Thread sadCounter = new Thread(new SadCounter());

		happyCounter.start();
		sadCounter.start();

	}
}

class HappyCounter implements Runnable {

	int count = 0;

	@Override
	public void run() {
		while (true) {
			count++;
			System.out.println("Happy " + count);
		}
	}
}

class SadCounter implements Runnable {

	int count = 0;

	@Override
	public void run() {
		while (true) {
			Thread.yield();
			count++;
			System.out.println("Sad " + count);
		}
	}
}
