package lesson190808.daemon;

public class MainDaemonExample {

	public static void main(String[] args) {
		Thread.currentThread().setDaemon(true);

		CounterThread counterThread = new CounterThread();

		counterThread.start();

		for (int i = 0; i < 100; i++) {
			System.out.println("main " + i);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
