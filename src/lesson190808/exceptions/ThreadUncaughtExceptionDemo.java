package lesson190808.exceptions;

class ThreadExceptionHandler implements Thread.UncaughtExceptionHandler {

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		System.out.println(t + " throws exception: " + e);
	}
}

public class ThreadUncaughtExceptionDemo {

	public static void main(String[] args) {
		Thread t = new Thread(new SimpleThread());
		t.setName("ExceptionalThread");
		t.setUncaughtExceptionHandler(new ThreadExceptionHandler());
		t.start();
	}
}

class SimpleThread implements Runnable {
	@Override
	public void run()  {
		hello();
	}

	public void hello() {
		run();
	}
}
