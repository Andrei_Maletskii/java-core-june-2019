package lesson190808.interrupted;

class Watcher extends Thread {
	Thread threadToWatch;

	public Watcher(Thread threadToWatch) {
		this.threadToWatch = threadToWatch;
	}

	@Override
	public void run() {
		while (true) {
			Thread.interrupted();
			threadToWatch.isInterrupted();
			System.out.println(threadToWatch.isInterrupted());
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

public class InterruptExample {
	public static void main(String[] args) throws InterruptedException {
		Thread threadToInterrupt = new Thread(new Runnable() {
			@Override
			public void run() {
				int i = 0;

				for (;;) {

				}

			}
		});

		Watcher watcher = new Watcher(threadToInterrupt);

		threadToInterrupt.start();
		watcher.start();
		Thread.sleep(500);
		threadToInterrupt.interrupt();
		System.out.println("interrupted 1 times");
//		Thread.sleep(500);
//		threadToInterrupt.interrupt();
//		System.out.println("interrupted 2 times");
//		Thread.sleep(500);
//		threadToInterrupt.interrupt();
//		System.out.println("interrupted 3 times");
	}
}
