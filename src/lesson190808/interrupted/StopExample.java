package lesson190808.interrupted;

public class StopExample {
	public static void main(String[] args) throws InterruptedException {
		Thread threadToInterrupt = new Thread(new Runnable() {
			@Override
			public void run() {
				int i = 0;

				try {
					for (;;) {
						System.out.println("hello " + i++);
						Thread.sleep(100);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				System.out.println("close resource");
			}
		});

		threadToInterrupt.start();
		Thread.sleep(500);
		threadToInterrupt.suspend();
		System.out.println("suspended");
		Thread.sleep(5000);
		threadToInterrupt.resume();
		System.out.println("resumed");
		Thread.sleep(500);
		threadToInterrupt.stop();
		System.out.println("stopped");

	}
}
