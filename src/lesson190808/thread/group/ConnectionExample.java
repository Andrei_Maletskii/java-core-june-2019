package lesson190808.thread.group;

public class ConnectionExample {

	public static void main(String[] args) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				for (;;) {
					System.out.println("hello");
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		thread.start();

		System.out.println(thread.getThreadGroup());
		System.out.println(Thread.currentThread().getThreadGroup());

		throw new RuntimeException();
	}
}
