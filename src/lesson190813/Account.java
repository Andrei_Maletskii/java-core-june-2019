package lesson190813;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class Account {
	private final Queue<Integer> linkedList = new LinkedList<>();
	final HashMap<String, String> map = new HashMap<>();
	private final static Object staticLock = new Object();
	private final Object lock = new Object();

	{
		synchronized(lock) {

		}

		synchronized(this) {

		}
	}

	static {
		synchronized(Account.class) {

		}

		synchronized(staticLock) {

		}
	}

	public void offer(Integer i) {
		synchronized(linkedList) {
			linkedList.offer(i);
		}
	}
	public Integer poll() {
		synchronized(linkedList) {
			return linkedList.poll();
		}
	}
	public void put(String key, String value) {
		synchronized(map) {
			map.put(key, value);
		}
	}
	public String get(String key) {
		synchronized(map) {
			return map.get(key);
		}
	}

	public void method() {
		synchronized(map) {
			synchronized(linkedList) {
				Integer poll = linkedList.poll();
				map.put(poll.toString(), poll.toString());
			}
		}
	}

	public void method1(Integer integer) {
		synchronized(linkedList) {
			synchronized(map) {
				String s = map.get(integer.toString());
				linkedList.add(Integer.parseInt(s));
			}
		}
	}

	public static synchronized int calc(int a, int b) {
		return a + b;
	}

	public static synchronized int hello(int a, int b) {
		return a + b;
	}

	public static int calc() {
		Class<Account> accountClass = Account.class;

		synchronized(staticLock) {

		}

		return 0;
	}

	public static int calc1() {
		Class<Account> accountClass = Account.class;

		synchronized(staticLock) {

		}

		return 0;
	}

	public static void main(String[] args) {
		Account account1 = new Account();
		Account account2 = new Account();

		Thread thread1 = new Thread(new Runnable() {
			@Override
			public void run() {
				account1.linkedList.add(1);
				account1.put("1", "1");
			}
		});

		Thread thread2 = new Thread(new Runnable() {
			@Override
			public void run() {
				account2.put("1", "1");
			}
		});
	}
}
