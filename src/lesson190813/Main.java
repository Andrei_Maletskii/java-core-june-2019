package lesson190813;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
	public static void main(String[] args) {
		Object object = new Object();
		int i = 0;

		i++;

		AtomicInteger atomicInteger = new AtomicInteger(0);

		i++;
		++i;

		LinkedList<Integer> linkedList = new LinkedList<>();

		synchronized(linkedList) {

		}

		System.out.println(i);
		System.out.println(object);
		i = 5;
		object = new Object();

		// read i
		// i + 1
		// write i

		System.out.println(i);
	}
}
