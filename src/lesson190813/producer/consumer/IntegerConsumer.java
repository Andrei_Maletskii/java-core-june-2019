package lesson190813.producer.consumer;

import java.util.ArrayList;
import java.util.List;

public class IntegerConsumer implements Runnable {

	ArrayList<Integer> store = new ArrayList<>();
	MyBlockingLinkedQueue queue;

	public IntegerConsumer(MyBlockingLinkedQueue queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		while (true) {
			Integer integer = null;
			try {
				integer = queue.poll();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

			if (integer.equals(Integer.MIN_VALUE)) {
				return;
			}

			store.add(integer);
		}
	}

	public List<Integer> getStore() {
		return store;
	}
}
