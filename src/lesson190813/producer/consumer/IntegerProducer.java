package lesson190813.producer.consumer;

import java.util.Random;

public class IntegerProducer implements Runnable {

	private MyBlockingLinkedQueue queue;
	private int count;
	private int bound;

	public IntegerProducer(MyBlockingLinkedQueue queue, int count, int bound) {
		this.queue = queue;
		this.count = count;
		this.bound = bound;
	}

	@Override
	public void run() {
		Random random = new Random();

		for (int i = 0; i < count; i++) {
			try {
				queue.put(random.nextInt(bound));
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
