package lesson190813.producer.consumer;

import java.util.LinkedList;
import java.util.Queue;

public class MyBlockingLinkedQueue {

	private final Queue<Integer> internalQueue = new LinkedList<>();
	private final int capacity;

	public MyBlockingLinkedQueue(int capacity) {
		this.capacity = capacity;
	}

	public void put(Integer i) throws InterruptedException {
		synchronized(internalQueue) {
			while (internalQueue.size() >= capacity) {
				System.out.println("The queue is full, waiting...");
				internalQueue.wait();
			}

			internalQueue.offer(i);
			internalQueue.notify();
		}
	}

	public Integer poll() throws InterruptedException {
		synchronized(internalQueue) {
			while (internalQueue.size() == 0) {
				System.out.println("The queue is empty, waiting...");
				internalQueue.wait();
			}

			Integer integer = internalQueue.poll();
			internalQueue.notify();
			return integer;
		}
	}

	public static void main(String[] args) throws InterruptedException {
		MyBlockingLinkedQueue myQueue = new MyBlockingLinkedQueue(10);

		IntegerConsumer integerConsumer1 = new IntegerConsumer(myQueue);
		IntegerConsumer integerConsumer2 = new IntegerConsumer(myQueue);
		IntegerProducer integerProducer1 = new IntegerProducer(myQueue, 1000, 1000);
		IntegerProducer integerProducer2 = new IntegerProducer(myQueue, 1000, 1000);

		Thread producer1 = new Thread(integerProducer1);
		Thread producer2 = new Thread(integerProducer2);
		Thread consumer1 = new Thread(integerConsumer1);
		Thread consumer2 = new Thread(integerConsumer2);

		producer1.start();
		producer2.start();
		consumer1.start();
		consumer2.start();

		producer1.join();
		producer2.join();

		myQueue.put(Integer.MIN_VALUE);
		myQueue.put(Integer.MIN_VALUE);

		consumer1.join();
		consumer2.join();

		System.out.println("Store size 1 = " + integerConsumer1.getStore().size());
		System.out.println("Store size 2 = " + integerConsumer2.getStore().size());


	}
}
