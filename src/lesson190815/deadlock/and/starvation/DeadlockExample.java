package lesson190815.deadlock.and.starvation;

public class DeadlockExample {

	public static final int N = 100;

	public static void main(String[] args) {
		Object fork = new Object();
		Object knife = new Object();

		FirstHungryMan first = new FirstHungryMan(fork, knife);
		SecondHungryMan second = new SecondHungryMan(fork, knife);

		first.start();
		second.start();
	}
}

class FirstHungryMan extends Thread {

	private final Object fork;
	private final Object knife;

	public FirstHungryMan(Object fork, Object knife) {
		this.fork = fork;
		this.knife = knife;
	}

	@Override
	public void run() {
		for (int i = 0; i < DeadlockExample.N; i++) {
			synchronized(fork) {
				synchronized(knife) {
					System.out.println("First man eating...");
				}
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		System.out.println("First man finished eating");
	}

}

class SecondHungryMan extends Thread {

	private final Object fork;
	private final Object knife;

	public SecondHungryMan(Object fork, Object knife) {
		this.fork = fork;
		this.knife = knife;
	}

	@Override
	public void run() {
		for (int i = 0; i < DeadlockExample.N; i++) {
			synchronized(fork) {
				synchronized(knife) {
					System.out.println("Second man eating...");
				}
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println("Second man finished eating");

	}
}


