package lesson190815.livelock;

public class Livelock {

	public static void main(String[] args) {
		final Worker worker1 = new Worker("Worker 1 ", true);
		final Worker worker2 = new Worker("Worker 2", true);

		final CommonResource commonResource = new CommonResource(worker1);

		new Thread(new Runnable() {
			@Override
			public void run() {
				worker1.work(commonResource, worker2);
			}
		}).start();

		new Thread(new Runnable() {
			@Override
			public void run() {
				worker2.work(commonResource, worker1);
			}
		}).start();
	}
}
