package lesson190815.locks;

import java.util.concurrent.locks.ReentrantLock;

public class ObjectToSync {

	String string = "hello";
	ReentrantLock reentrantLock = new ReentrantLock();
	ReentrantLock reentrantLock1 = new ReentrantLock();

	public void enrichString() {
		concat0();
		concat1();
	}

	private void concat0() {
		reentrantLock.lock();

		string = string + "hello1";
	}

	private void concat1() {
		string = string + string;

		reentrantLock.unlock();
	}


	public static void main(String[] args) {
		ObjectToSync obj = new ObjectToSync();

//		obj.concat0();


		obj.concat1();
	}
}
