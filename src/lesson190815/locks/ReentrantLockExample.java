package lesson190815.locks;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class Mutable {

	ReentrantLock reentrantLock;

	public Mutable(ReentrantLock reentrantLock) {
		this.reentrantLock = reentrantLock;
	}

	public void methodLock() {
		reentrantLock.lock();

//		throw new RuntimeException();
	}

	public void methodUnlock() {
		reentrantLock.unlock();

		System.out.println("unlocked");
	}

	public void tryPrint() {
		if (!reentrantLock.tryLock()) {
			System.out.println("monitor is locked");
			return;
		}

		System.out.println("tryPrint");

		reentrantLock.unlock();
	}
}

public class ReentrantLockExample {

	public static void main(String[] args) throws InterruptedException {
		ReentrantLock reentrantLock = new ReentrantLock();
		Mutable mutable = new Mutable(reentrantLock);

		new Thread(new Runnable() {
			@Override
			public void run() {
				mutable.methodLock();
			}
		}).start();

		Thread.sleep(3000);

		mutable.methodUnlock();

		mutable.tryPrint();

		ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();

		ReentrantReadWriteLock.WriteLock writeLock = reentrantReadWriteLock.writeLock();
		ReentrantReadWriteLock.ReadLock readLock = reentrantReadWriteLock.readLock();

		writeLock.lock();
		readLock.lock();

	}
}
