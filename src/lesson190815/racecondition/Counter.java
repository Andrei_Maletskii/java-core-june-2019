package lesson190815.racecondition;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

public class Counter {

	private AtomicInteger atomicInteger = new AtomicInteger(0);
	private AtomicLong atomicLong = new AtomicLong(0);

	public int getCount() {
		return atomicInteger.get();
	}

	public void increment() {
		atomicInteger.getAndIncrement();
	}

	public void setAtomicInteger(AtomicInteger atomicInteger) {
		this.atomicInteger = atomicInteger;
	}

	public static void main(String[] args) {
		AtomicInteger atomicInteger = new AtomicInteger(0);
		int i = atomicInteger.incrementAndGet(); // ++i
		int j = atomicInteger.getAndIncrement(); // i++


		synchronized(Counter.class) {




		}

		ReentrantLock reentrantLock = new ReentrantLock();

		reentrantLock.lock();

		reentrantLock.unlock();


	}
}
