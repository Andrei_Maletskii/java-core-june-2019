package lesson190815.visibility;

public class Clicker extends Thread {
	int click = 0;
	private boolean running = true; // MAKE volatile

	public void run() {
		while (running) {
			click++;
		}
	}

	public void stopClick() {
		running = false;
	}

	public boolean isRunning() {
		return running;
	}

	public static void main(String[] args) throws InterruptedException {
		Clicker clicker = new Clicker();
		clicker.start();

		Thread.sleep(500);

		clicker.stopClick();

		Thread.sleep(500);

		System.out.println(clicker.isRunning());
	}
}
