package lesson190815.visibility;

import java.util.List;

public class Main {
	public static void main(String[] args) throws InterruptedException {
		VolatileObject volatileObject = new VolatileObject();

		ValueChecker valueChecker = new ValueChecker(volatileObject);

		Thread thread = new Thread(valueChecker);

		thread.start();

		List<Integer> integers = List.of(5, 7, 9, 3, 10, 500, -1);

		Thread.sleep(500);

		for (Integer integer : integers) {
			System.out.println("Setting new value to volatile object: " + integer);
			volatileObject.setValue(integer);
			Thread.sleep(500);
		}
	}
}
