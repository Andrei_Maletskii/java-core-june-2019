package lesson190815.visibility;

public class ValueChecker implements Runnable {

	private VolatileObject object;

	public ValueChecker(VolatileObject object) {
		this.object = object;
	}

	@Override
	public void run() {
		int value = object.getValue();
		while (true) {

			if (value == -1) {
				System.out.println("Work is completed");
				break;
			}

			int newValue = object.getValue();

			if (value != newValue) {
				value = newValue;

				System.out.println("Got value from volatile object = " + value);
			}

		}
	}
}
