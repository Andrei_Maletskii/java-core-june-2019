package lesson190815.visibility;

public class VolatileObject {

	private volatile int value = 0;

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
