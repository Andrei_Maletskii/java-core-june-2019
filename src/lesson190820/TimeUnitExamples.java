package lesson190820;

import lesson190613.Fibonacci;

import java.util.concurrent.TimeUnit;

public class TimeUnitExamples {

	public static void main(String[] args) throws InterruptedException {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				long result = Fibonacci.fib1(45);
				System.out.println(result);
			}
		});

		thread.start();

		TimeUnit.SECONDS.timedJoin(thread, 15);
		TimeUnit.SECONDS.timedWait(new Object(), 5);

		System.out.println("Exit");
	}
}
