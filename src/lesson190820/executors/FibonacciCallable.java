package lesson190820.executors;

import lesson190613.Fibonacci;

import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Long> {

	private int i;

	public FibonacciCallable(int i) {
		this.i = i;
	}

	@Override
	public Long call() throws Exception {
		System.out.println("Fib " + i + " started");

		long result = Fibonacci.fib1(i);

		System.out.println("Fib " + i + " calculated");

		return result;
	}
}

