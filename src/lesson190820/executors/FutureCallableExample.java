package lesson190820.executors;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureCallableExample {

	public static void main(String[] args) throws ExecutionException, InterruptedException {
		Thread.sleep(10000);

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		List<Future<Long>> futureList = new ArrayList<>();

		for (int i = 0; i < 50; i++) {
			Future<Long> future = executorService.submit(new FibonacciCallable(i));

			futureList.add(future);
		}

		System.out.println("Start");

		for (int i = 0; i < futureList.size(); i++) {
			Future<Long> fibonacciNumber = futureList.get(i);

			System.out.println(i + " fibonacci number is = " + fibonacciNumber.get());
		}

		System.out.println("Exit");

		executorService.shutdown();

	}
}
