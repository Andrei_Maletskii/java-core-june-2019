package lesson190820.executors;

import lesson190613.Fibonacci;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FutureExamples2 {

	public static void main(String[] args) throws InterruptedException {
		ExecutorService service = Executors.newSingleThreadExecutor();

		Future<Long> fibonacciNumber = service.submit(new Callable<Long>() {
			@Override
			public Long call() throws Exception {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					return 0l;
				}

				return Fibonacci.fib1(100);
			}
		});

		Thread.sleep(1000);

		System.out.println(fibonacciNumber.isDone());

		Thread.sleep(1000);

		fibonacciNumber.cancel(true);

		Thread.sleep(1000);

		System.out.println(fibonacciNumber.isCancelled());

		service.shutdownNow();


	}
}
