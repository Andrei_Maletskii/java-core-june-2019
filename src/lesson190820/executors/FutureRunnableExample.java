package lesson190820.executors;

import lesson190613.Fibonacci;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class FutureRunnableExample {

	public static void main(String[] args) throws InterruptedException {
		ExecutorService executorService = Executors.newFixedThreadPool(2);

		Future<?> future = executorService.submit(new Runnable() {
			@Override
			public void run() {
				System.out.println(Fibonacci.fib1(10));
			}
		});

		System.out.println("hello");

		try {
			future.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

		System.out.println("Exit");

		executorService.shutdown();
	}
}
