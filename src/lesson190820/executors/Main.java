package lesson190820.executors;

import java.time.ZonedDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class SimpleCommand implements Runnable {

	private int count;

	public SimpleCommand(int count) {
		this.count = count;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < count; i++) {
			sb.append("hello");
		}

		System.out.println(count + " " + sb.toString());
	}
}

public class Main {

	public static void main(String[] args) {
//		ExecutorService executor = Executors.newSingleThreadExecutor();
//		ExecutorService executor = Executors.newFixedThreadPool(2);
//		ExecutorService executor = Executors.newFixedThreadPool(
//			Runtime.getRuntime().availableProcessors()
//		);
//		ExecutorService executor = Executors.newCachedThreadPool();
		ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

		scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(7000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				System.out.println(ZonedDateTime.now());
			}
		}, 0, 5, TimeUnit.SECONDS);

	}
}
