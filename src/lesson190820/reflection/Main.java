package lesson190820.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

public class Main {
	public static void main(String[] arguments)
		throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException,
		InstantiationException {
		if (arguments.length < 3) {
			System.err.println("Usage: ClassName, methodName, argument");
			return;
		}

		String className = arguments[0];
		String methodName = arguments[1];
		boolean argument = Boolean.parseBoolean(arguments[2]);

		Class<?> targetClass = Class.forName(className);
		Annotation[] annotations = targetClass.getAnnotations();

		Constructor<?> declaredConstructor = targetClass.getDeclaredConstructor();

		declaredConstructor.setAccessible(true);

		Object targetInstance = declaredConstructor.newInstance();

//		TargetClass targetInstance1 = (TargetClass) targetInstance;
//		targetInstance1.targetMethod(); ERROR

		Class<Double> doubleClass = double.class;
		Class<Boolean> booleanClass = boolean.class;
		Class<Boolean> booleanClass1 = Boolean.class;
		Class<Boolean> type = Boolean.TYPE;
		Method method = targetClass.getDeclaredMethod(methodName, booleanClass);

		method.setAccessible(true);

		Object result = method.invoke(targetInstance, argument);

		System.out.println(result);


	}
}
