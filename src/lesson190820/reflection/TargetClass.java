package lesson190820.reflection;

public class TargetClass {

	private TargetClass() {}

	private String targetMethod(boolean flag) {
		if (flag) {
			return "The boolean flag is true, congrats!";
		}

		return "The boolean flag is false, that's sad";
	}

	private String targetMethod(Boolean flag) {
		if (flag) {
			return "The Boolean flag is true, congrats!";
		}

		return "The Boolean flag is false, that's sad";
	}
}
