package lesson190822;

public class AnonymousExample {

	public static void main(String[] args) {

		Runnable procedure = new Runnable() {
			@Override
			public void run() {
				System.out.println("hello");
			}
		};


		process(procedure);

		Thread thread = new Thread(procedure);
		thread.start();
	}

	private static void process(Runnable runnable) {
		runnable.run();
	}
}
