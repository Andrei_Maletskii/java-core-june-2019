package lesson190822;

public class AnonymousVsLambda {

	public static void main(String[] args) {


		Runnable anonymous = new Runnable() {
			@Override
			public void run() {
				System.out.println("hello");
			}
		};

		Runnable lambda = () -> System.out.println("hello");

		anonymous.run();
		lambda.run();

		System.out.println(anonymous);
		System.out.println(lambda);
	}
}
