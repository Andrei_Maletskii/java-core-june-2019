package lesson190822;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;

public class ComparatorExamples {
	public static void main(String[] args) {
		List<Person> personList = new ArrayList<>();

		personList.add(new Person("Jane", 20));
		personList.add(new Person("Michael", 30));
		personList.add(new Person("John", 34));
		personList.add(new Person("Robert", 50));
		personList.add(new Person("Kate", 25));

		Collections.sort(personList, (p1, p2) -> p1.getName().compareTo(p2.getName()));



		Collections.sort(personList,
			Comparator.comparing(Person::getName)
				.thenComparingInt(Person::getAge)
		);


		BiFunction<String, String, String> biFunction = (String s1, String s2) -> s1.concat(s2);


	}

}

class Person {
	private final String name;
	private final int age;

	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}
}
