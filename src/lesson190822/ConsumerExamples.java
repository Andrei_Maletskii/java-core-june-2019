package lesson190822;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ConsumerExamples {

	public static void main(String[] args) {
		Consumer<Integer> println = System.out::println;


		List<Integer> integers = List.of(1, 2, 3, 4, 5);

		integers.forEach(println);
		integers.forEach(System.out::println);

		BiConsumer<String, Integer> biConsumer = (str, i) -> {
			for (int j = 0; j < i; j++) {
				System.out.println(str);
			}
		};

		biConsumer.accept("hello", 5);
	}
}
