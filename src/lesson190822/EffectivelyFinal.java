package lesson190822;

public class EffectivelyFinal {




}

class X {
	static int staticInt = 10;
	int instanceInt = 20;

	void m(Object arg) {
		int localInt = 10;
		Runnable anonymous = new Runnable() {
			@Override
			public void run() {
				// int arg = arg
				// int localInt = localInt
				System.out.println(X.staticInt);
				System.out.println(X.this.instanceInt);
				System.out.println(arg);
				System.out.println(localInt);
//				arg = new Object();
				try {
					arg.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		Runnable lambda = () -> {
			System.out.println(X.staticInt);
			System.out.println(this.instanceInt);
			System.out.println(arg);
			System.out.println(localInt);
		};


		staticInt++;
		instanceInt++;
//		arg++;		//	need to be effectively final
//		localInt++;	//	need to be effectively final
	}
}


