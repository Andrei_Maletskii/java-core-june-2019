package lesson190822;

public class EffectivlyFinalAdditionalExample {

	public static void main(String[] args) {
		int x = 10;

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println(x);
			}
		};

		Thread thread = new Thread(runnable);
		thread.start();

		System.out.println("End of main");
	}
}
