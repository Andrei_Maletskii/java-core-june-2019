package lesson190822;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class FunctionExamples {

	public static void main(String[] args) {
		Function<String, Integer> getLength = new Function<String, Integer>() {
			@Override
			public Integer apply(String s) {
				return s.length();
			}
		};

		Function<String, Integer> getLength1 = (String s) -> {
			return s.length();
		};

		Function<String, Integer> getLength2 = s -> s.length();

		Function<String, Integer> getLength3 = String::length;

		BiFunction<String, String, String> concat = (s0, s1) -> s0.concat(s1);

		BiFunction<String, String, String> concat1 = String::concat;

		Map<String, String> dictionary = new HashMap<>();

		dictionary.put("hi", "Halo");
		dictionary.put("good morning", "Guten morgen");
		dictionary.put("Добрый день", "Guten tag");

		BinaryOperator<String> binaryOperator = 		(s0, s1) -> s1.toLowerCase();
		BiFunction<String, String, String> biFunction = (s0, s1) -> s1.toLowerCase();

		dictionary.replaceAll((s0, s1) -> s1.toLowerCase());

		System.out.println(dictionary);

		String result = binaryOperator.apply("String", "Hello");
		System.out.println(result);

		UnaryOperator<String> unaryOperator = String::toLowerCase;
		Function<String, String> function = String::toLowerCase;
	}
}
