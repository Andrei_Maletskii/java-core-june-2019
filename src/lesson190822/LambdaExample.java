package lesson190822;

public class LambdaExample {

	public static void main(String[] args) {

		Runnable procedure = () -> System.out.println("hello");


		process(procedure);

		Thread thread = new Thread(procedure);
		thread.start();
	}

	private static void process(Runnable runnable) {
		runnable.run();
	}
}
