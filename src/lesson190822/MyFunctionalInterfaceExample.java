package lesson190822;

import java.util.concurrent.Callable;

public class MyFunctionalInterfaceExample {

	public static void main(String[] args) {


		MyFunctionalInterface mfi = () -> System.out.println("hello");

		Runnable runnable = () -> System.out.println("hi1");

//		MyAbstract ma = () -> System.out.println("111"); ERROR
	}
}


@FunctionalInterface
interface MyFunctionalInterface {

	void method();
}

abstract class MyAbstract implements MyFunctionalInterface {

}


