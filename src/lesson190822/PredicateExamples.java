package lesson190822;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PredicateExamples {

	public static void main(String[] args) {
		Predicate<String> stringPredicate = new Predicate<String>() {
			@Override
			public boolean test(String s) {
				return s.isEmpty();
			}
		};

		Predicate<String> stringPredicate1 = (String s) -> {
			return s.isEmpty();
		};

		Predicate<String> stringPredicate2 = (String s) -> s.isEmpty();

		Predicate<String> stringPredicate3 = (s) -> s.isEmpty();

		Predicate<String> stringPredicate4 = s -> s.isEmpty();

		Predicate<String> stringPredicate5 = String::isEmpty;

		System.out.println(stringPredicate5.test(""));
		System.out.println(stringPredicate5.test("hello"));

		BiPredicate<String, String> biStringPredicate = new BiPredicate<String, String>() {
			@Override
			public boolean test(String s, String s2) {
				return s.equals(s2);
			}
		};

		BiPredicate<String, String> biStringPredicate1 = (String s, String s2) -> {
			return s.equals(s2);
		};

		BiPredicate<String, String> biStringPredicate2 = (String s, String s2) -> s.equals(s2);

		BiPredicate<String, String> biStringPredicate3 = (s, s2) -> s.equals(s2);

		BiPredicate<String, String> biStringPredicate4 = String::equals;

		System.out.println(biStringPredicate4.test("hello", "hello"));
		System.out.println(biStringPredicate4.test("hello", "hi"));

		// String.method(String)

	}
}
