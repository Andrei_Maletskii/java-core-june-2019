package lesson190822;

import java.io.IOException;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

public class PrimitiveFunctionsExamples {

	public static void main(String[] args) {
		ToIntFunction<String> stringToIntLength = String::length;
		int hello = stringToIntLength.applyAsInt("hello");

		List<String> strings = new ArrayList<>();

		strings.add("1");
		strings.add("2");
		strings.add("hello");
		strings.add("4");
		strings.add("5");

		IntStream intStream = strings.stream().mapToInt(stringToIntLength);

		IntSummaryStatistics intSummaryStatistics = intStream.summaryStatistics();
		System.out.println(intSummaryStatistics);

		IntSummaryStatistics intSummaryStatistics1 = strings.stream().mapToInt(s -> {
			try {
				return Integer.parseInt(s);
			} catch (NumberFormatException | OutOfMemoryError e) {
				return 0;
			}
		}).summaryStatistics();

		System.out.println(intSummaryStatistics1);


	}
}
