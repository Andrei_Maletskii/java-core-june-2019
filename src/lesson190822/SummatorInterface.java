package lesson190822;


@FunctionalInterface
public interface SummatorInterface {

	int sum(int a, int b, int c);
}
