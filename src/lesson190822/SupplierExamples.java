package lesson190822;

import java.util.function.Consumer;
import java.util.function.Supplier;

public class SupplierExamples {

	public static void main(String... args) {
		Supplier<Car> carSupplier = new Supplier<Car>() {
			@Override
			public Car get() {
				return new Car();
			}
		};

		Supplier<Car> carSupplier1 = () -> {
			System.out.println("hello");

			return new Car();
		};

		Supplier<Car> carSupplier2 = () -> new Car();

		Supplier<Car> carSupplier3 = Car::new;

		Consumer<Car> drive = Car::drive;

		driveTenCars(Car::new);
	}

	static void driveTenCars(Supplier<Car> carSupplier) {
		for (int i = 0; i < 10; i++) {
			carSupplier.get().drive();
		}
	}
}
class Car {

	void drive() {
		System.out.println("Driving " + this);
	}
}
