package lesson190827;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;
import java.util.concurrent.Flow;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class MethodReferenceExamples {


//	public static void main(String[] args) {
	public static void main(String... args) {
		//(args) -> ClassName.staticMethod(args)
		//ClassName::staticMethod

		Supplier<Thread> threadSupplier1 = Thread::currentThread;
		Supplier<Thread> threadSupplier2 = () -> Thread.currentThread();

		Function<Integer, String> func1 = (i) -> String.valueOf(i);
		Function<Integer, String> func2 = String::valueOf;

		IntArrayFunction intArrayFunction1 = (array -> Arrays.toString(array));
		IntArrayFunction intArrayFunction2 = Arrays::toString;

		//(arg, rest...)  -> arg.instanceMethod(rest) "..." - not vararg!
		//ClassName::instanceMethod

		Comparator<String> stringComparator1 = (s1, s2) -> s1.compareTo(s2);
		Comparator<String> stringComparator2 = String::compareTo;

		BiFunction<Object, Object, Boolean> equals1 = (o1, o2) -> o1.equals(o2);
		BiPredicate<Object, Object> equals2 = (o1, o2) -> o1.equals(o2);
		BiPredicate<Object, Object> equals3 = Object::equals;

		//(args)  ->  expr.instanceMethod(args)
		//expr::instanceMethod

		Consumer<Object> objectConsumer1 = o -> System.out.println(o);
		Consumer<Object> objectConsumer2 = System.out::println;

		Supplier<String> stringSupplier1 = () -> Thread.currentThread().getName();
		Supplier<String> stringSupplier2 = Thread.currentThread()::getName;

		Supplier<String> strSupplier1 = () -> str.toUpperCase();
		Supplier<String> strSupplier2 = MethodReferenceExamples.str::toUpperCase;
		String s = strSupplier2.get();

		UnaryOperator<String> op1 = String::toUpperCase;
		UnaryOperator<String> op2 = s1 -> s1.toUpperCase();

		str = "privet";

		final String localStr = "localHello";

		Supplier<String> toLowerCase1 = () -> localStr.toLowerCase();
		Supplier<String> toLowerCase2 = localStr::toLowerCase;

//		localStr = "privet"; must be final or effectively final

		//(args) -> new ClassName(args)
		//ClassName::new

		Supplier<MethodReferenceExamples> supplier = MethodReferenceExamples::new;

		Supplier<Person> personSupplier = Person::new;
		Function<String, Person> personFunction = Person::new;
	}

	private static String str = "hello";
}

@FunctionalInterface
interface IntArrayFunction {

	public abstract String applyAsIntArray(int[] array);

}

class Person {
	private String name;

	public Person(String name) {
		this.name = name;
	}

	public Person() {
		this.name = "Ann";
	}
}
