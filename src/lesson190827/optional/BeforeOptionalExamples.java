package lesson190827.optional;

import java.util.Objects;
import java.util.Random;

public class BeforeOptionalExamples {

	final static Random random = new Random();

	public static void main(String[] args) {

		String name = getName();

		if (name == null) {
			System.out.println("Name is null");
		}

		if (Objects.isNull(name)) {
			System.out.println("Name is null!");
		}

		Objects.requireNonNull(name, "Name is null");

	}

	private static String getName() {
		return random.nextBoolean() ? "Robert" : null;
	}

	public static void processName(String name) {
		Objects.requireNonNull(name, "Name is null");

		// processing...
	}
}
