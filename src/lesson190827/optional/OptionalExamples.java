package lesson190827.optional;

import java.util.Optional;
import java.util.Random;

public class OptionalExamples {

	final static Random random = new Random();

	public static void main(String[] args) {
		Optional<String> optionalName = getName();

//		if (optionalName.isPresent()) {
//			System.out.println(optionalName.get());
//		}

		String name = optionalName.orElse(getDefaultName());
		System.out.println(name);

		String name1 = optionalName.orElseGet(OptionalExamples::getDefaultName);
		System.out.println(name1);

		optionalName.ifPresent(System.out::println);


	}

	private static String getDefaultName() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Getting default name");

		return "Kate";
	}

	private static Optional<String> getName() {
//		String name = random.nextBoolean() ? "Robert" : null;

		return Optional.ofNullable("Robert");
	}
}
