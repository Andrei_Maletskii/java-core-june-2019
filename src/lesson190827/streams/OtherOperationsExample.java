package lesson190827.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OtherOperationsExample {

	public static void main(String[] args) {
		Stream<String> bananaStream = Stream.of("b", "a", "n", "a", "n", "a");
		List<String> banana = Arrays.asList("b", "a", "n", "a", "n", "a");

		// ba na na -> bana na -> banana
		// b a n a
		// nanaba

		long count = banana.stream().count();

		System.out.println(count);

		Stream<String> stream = banana.stream();

		Optional<String> reduce = stream.reduce((s1, s2) -> s1 + s2);
		reduce.ifPresent(System.out::println);

		Optional<Integer> sum = Stream.of(1, 2, 3, 4, 5).reduce(Integer::sum);
		sum.ifPresent(System.out::println);

		Optional<Integer> product = Stream.of(1, 2, 3, 4, 5).reduce((i1, i2) -> i1 * i2);
		product.ifPresent(System.out::println);


		String myBanana = Stream.of("b", "a", "n", "a", "n", "a").reduce("My ", String::concat);
		System.out.println(myBanana);

		//		String myBanana2 = Stream.of("b", "a", "n", "a", "n", "a").reduce(null, String::concat);
		//		System.out.println(myBanana2);

		StringBuilder reduce1 = Stream.of("b", "a", "n", "a", "n", "a")
			.reduce(
				new StringBuilder(),
				StringBuilder::append,
				StringBuilder::append
			);

		System.out.println(reduce1.append(", HELLO!"));


	}
}
