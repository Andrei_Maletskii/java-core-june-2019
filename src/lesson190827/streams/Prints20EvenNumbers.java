package lesson190827.streams;

import java.util.ArrayList;
import java.util.stream.Stream;

public class Prints20EvenNumbers {


	public static void main(String[] args) {
		// 1. get stream from source / generate stream
//		Stream<Integer> stream = Stream.iterate(0, n -> n + 2);

		// 2. use transformations
//		Stream<Integer> limited = stream.limit(20);

		// 3. terminal operation
//		limited.forEach(System.out::println);

		Stream.iterate(0, n -> n + 2).limit(20).forEach(System.out::println);



		for (int i = 0; i < 40; i+=2) {
			System.out.println(i);
		}

	}
}
