package lesson190827.streams;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsExamples {

	public static void main(String[] args) {

		Stream<String> empty = Stream.empty();
		Stream<Integer> integers = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9);

		Optional<Integer> anyInteger = integers.findAny();

		anyInteger.ifPresent(System.out::println);


		Optional<Object> any = Stream.empty().findAny();
		any.ifPresent(System.out::println);

		Optional<Integer> first = Stream.of(1, 2, 3, 4, 5).findFirst();

		Stream<Integer> skippedLimited = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9).skip(3).limit(3);

		skippedLimited.forEach((i) -> System.out.print(i + " "));
		System.out.println();

		// print 3 middle integers with space between them
		String collected = Stream.of(1, 2, 3, 4, 5)
			.skip(1)
			.limit(3)
			.map(Objects::toString)
			.collect(Collectors.joining(" "));

		System.out.println(collected);

	}
}
