package lesson190829.optional;

import java.util.Optional;

public class OptionalExamplesAll {
	public static void main(String[] args) {
		printPersonInfo(new Person());

		Person person = new Person();
		Car car = new Car();

		car.insurance = Optional.of(new Insurance("Alpha"));
		person.car = Optional.of(car);

		printPersonInfo(person);
	}

	private static void printPersonInfo(Person person) {
		Optional<Car> optionalCar = person.getCar();

		optionalCar.ifPresent(System.out::println);

//		System.out.println(currentCar);

//		Optional<Optional<Insurance>> currentInsurance = optionalCar.map(Car::getInsurance);

		Optional<Insurance> currentInsurance = optionalCar.flatMap(Car::getInsurance);

		currentInsurance.ifPresentOrElse(
			System.out::println,
			() -> System.out.println("No Insurance!")
		);

		Optional<Insurance> renesance = currentInsurance
			.filter(insurance -> insurance.name.equals("Renesance"));

		renesance.orElseThrow(WrongInsuranceType::new);

		//		currentInsurance.ifPresent(insurance -> System.out.println(insurance.name));
	}
}


class Person {
	Optional<Car> car = Optional.empty();

	public Optional<Car> getCar() {
		return car;
	}
}

class Car {
	Optional<Insurance> insurance = Optional.empty();

	public Optional<Insurance> getInsurance() {
		return insurance;
	}
}

class Insurance {
	String name;

	public Insurance(String name) {
		this.name = name;
	}

	@Override public String toString() {
		return "Insurance{" +
			"name='" + name + '\'' +
			'}';
	}
}

class CarNotFound extends RuntimeException {

}

class WrongInsuranceType extends RuntimeException {

}


