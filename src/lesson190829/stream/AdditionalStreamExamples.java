package lesson190829.stream;

import java.util.Arrays;
import java.util.List;

public class AdditionalStreamExamples {

	public static void main(String[] args) {
		List<Integer> integerList = Arrays.asList(1, 2, 3, 4, 5);

		boolean allAreGreatherThanZero = integerList.stream().allMatch(i -> i > 0);

		System.out.println(allAreGreatherThanZero);

		boolean anyMatch = integerList.stream().anyMatch(i -> i % 5 == 0);

		System.out.println(anyMatch);
	}
}
