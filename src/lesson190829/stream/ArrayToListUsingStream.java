package lesson190829.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ArrayToListUsingStream {

	public static void main(String[] args) {
		int[] ints = new int[10];
		IntStream stream = Arrays.stream(ints);
		Stream<Integer> boxed = stream.boxed();
		List<Integer> collect = boxed.collect(Collectors.toList());
	}
}
