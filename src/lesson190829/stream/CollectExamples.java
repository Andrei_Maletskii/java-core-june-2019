package lesson190829.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectExamples {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("aaa", "bbb", "ccc", "aaaa", "bbbb", "ccccc");

		List<String> collectedList = strings.stream()
			.filter(s -> s.length() > 4)
			.collect(Collectors.toList());
		Set<String> collectedSet = strings.stream()
			.skip(1)
			.limit(3)
			.collect(Collectors.toSet());
		String collect = strings.stream().collect(Collectors.joining(","));
//		String.join()
		System.out.println(collectedList);
		System.out.println(collectedSet);
		System.out.println(collect);

		Map<Integer, List<String>> stringsByLength = strings
			.stream()
			.collect(Collectors.groupingBy(String::length));
		System.out.println(stringsByLength);

		StringBuilder sb = strings.stream().collect(
			StringBuilder::new,
			StringBuilder::append,
			StringBuilder::append
		);

		System.out.println(sb.append(" Hello").toString());
	}
}
