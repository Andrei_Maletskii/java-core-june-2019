package lesson190829.stream;

import java.util.stream.Stream;

public class DistinctExample {

	public static void main(String[] args) {
		Stream<Integer> integers = Stream.of(1, 1, 2, 2, 5, 7, 8);

		integers.distinct().forEach(System.out::println);

	}
}
