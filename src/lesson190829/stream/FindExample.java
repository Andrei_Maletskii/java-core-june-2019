package lesson190829.stream;

import java.util.stream.Stream;

public class FindExample {

	public static void main(String[] args) {
		Stream.of(1, 2, 3, 4, 5).findFirst().ifPresent(System.out::println);
		Stream.of(5, 2, 3, 4, 1).findAny().ifPresent(System.out::println);
	}
}
