package lesson190829.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class FlatMapExample {

	public static void main(String[] args) {
		List<Folder> folders = Arrays.asList(
			new Folder(
				Arrays.asList(
					new File("hello1"),
					new File("hello2"),
					new File("hello3")
				)
			),
			new Folder(
				Arrays.asList(
					new File("foobar1"),
					new File("foobar2"),
					new File("foobar3"),
					new File("foobar4")
				)
			)
		);

		int fileCount = folders.stream()
			.map(Folder::getFileList)
			.map(List::size)
			.mapToInt(Integer::intValue)
			.sum();


		long fileCount2 = folders.stream()
//			.map(Folder::getFileList)
//			.flatMap(List::stream)
			.flatMap(folder -> folder.getFileList().stream())
			.count();

		System.out.println(fileCount);
		System.out.println(fileCount2);
	}
}

class Folder {
	List<File> fileList;

	public Folder(List<File> fileList) {
		this.fileList = fileList;
	}

	public List<File> getFileList() {
		return fileList;
	}
}

class File {
	String name;

	public File(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
