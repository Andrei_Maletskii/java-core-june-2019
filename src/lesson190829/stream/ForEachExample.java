package lesson190829.stream;

import java.util.stream.Stream;

public class ForEachExample {

	public static void main(String[] args) {
//		Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).parallel().forEach(System.out::println);
//		System.out.println("---");
//		Stream.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9).parallel().forEachOrdered(System.out::println);

		Stream.of(0, 1, 2, 3)
			.parallel()
			.forEach(i -> System.out.println(
				Thread.currentThread().getName() + " i = " + i)
			);
	}
}
