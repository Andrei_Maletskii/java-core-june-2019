package lesson190829.stream;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.IntStream;

public class MapExamples {

	public static void main(String[] args) {
		List<String> text = Arrays.asList("hello", "world");

		Integer lettersCount1 = text
			.stream()
			.map(String::length)
			.reduce(
				0,
				Integer::sum
			);

		int lettersCount2 = text.stream()
			.mapToInt(String::length)
			.sum();
		//		Stream<Integer> boxed = intStream.boxed();

		System.out.println(lettersCount1);
		System.out.println(lettersCount2);

		IntStream intStream = text.stream().mapToInt(String::length);
		IntSummaryStatistics intSummaryStatistics = intStream.summaryStatistics();

		System.out.println(intSummaryStatistics);
	}
}
