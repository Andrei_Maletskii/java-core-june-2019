package lesson190829.stream;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class MapStreamExample {

	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();

		map.put("a", "1");
		map.put("b", "2");
		map.put("c", "3");
		map.put("d", "4");
		map.put("e", "5");

		Stream<String> values = map.values().stream();
		Stream<String> keys = map.keySet().stream();
		Set<Map.Entry<String, String>> entries = map.entrySet();

		Stream<Map.Entry<String, String>> stream = entries.stream();

		Stream<String> stringStream = stream.map(entry -> entry.getKey() + entry.getValue());

		stringStream.forEach(System.out::println);
	}
}
