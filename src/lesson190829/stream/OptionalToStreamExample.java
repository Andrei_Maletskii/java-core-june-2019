package lesson190829.stream;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class OptionalToStreamExample {
	public static void main(String[] args) {
		Person person = new Person();

		Optional<String> name = person.getName();

		Stream<String> stream1 = name.stream();

		Optional<List<Car>> optionalCars = person.getOptionalCars();

		// Optional.empty().stream() -> Stream.empty();
		// Optional.of(car) -> Stream.of(car)

		Stream<List<Car>> stream = optionalCars.stream();

//		Stream<Stream<Car>> streamStream = stream.map(Collection::stream);

		Stream<Car> carStream = stream.flatMap(Collection::stream);

	}

}

class Person {
	Optional<String> name = Optional.empty();
	Optional<List<Car>> optionalCars = Optional.empty();

	public Optional<List<Car>> getOptionalCars() {
		return optionalCars;
	}

	public Optional<String> getName() {
		return name;
	}
}

class Car {
	private String name;

	public Car(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}

