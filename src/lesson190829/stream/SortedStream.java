package lesson190829.stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class SortedStream {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("ca", "deeeeee", "abcdasdasd", "b");

		strings.stream().sorted().forEach(System.out::println);

		System.out.println("---");

		strings.stream().sorted(Comparator.comparing(String::length)).forEach(System.out::println);

	}
}
