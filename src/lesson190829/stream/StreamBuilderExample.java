package lesson190829.stream;

import java.util.stream.Stream;

public class StreamBuilderExample {

	public static void main(String[] args) {
		Stream<Integer> stream1 = Stream.<Integer>builder().add(1).add(2).add(3).build();

		Stream.Builder<Integer> builder = Stream.builder();
		builder.accept(1);
		builder.accept(2);
		builder.accept(3);
		Stream<Integer> stream2 = builder.build();

		Stream<Integer> concat = Stream.concat(stream1, stream2);


	}
}
