package lesson190829.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamMapFlatMapExample {
	public static void main(String[] args) {
		List<String> text = Arrays.asList("Hello", ",", " world", "!");

//		Stream<Stream<String>> streamStream = text.stream().map(s -> Arrays.stream(s.split("")));
		Stream<String> streamStream = text.stream().flatMap(s -> Arrays.stream(s.split("")));

		streamStream.forEach(System.out::println);
	}

}

