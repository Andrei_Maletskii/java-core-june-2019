package lesson190829.stream.processor;


import java.util.List;

class Computer {
	private List<Core> cores;

	public Computer(List<Core> cores) {
		this.cores = cores;
	}

	public List<Core> getCores() {
		return cores;
	}
}
