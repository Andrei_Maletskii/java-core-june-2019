package lesson190829.stream.processor;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ComputerUse {

	public static void main(String[] args) {
		Computer amdComputer = new Computer(
			Arrays.asList(
				new Core("amd-core-1"),
				new Core("amd-core-2"),
				new Core("amd-core-3"),
				new Core("amd-core-4")
			)
		);

		Computer intelComputer = new Computer(
			Arrays.asList(
				new Core("intel-core-1"),
				new Core("intel-core-2"),
				new Core("intel-core-3"),
				new Core("intel-core-4"),
				new Core("intel-core-5"),
				new Core("intel-core-6")
			)
		);
		//
		//		Stream<Computer> computerStream = Stream.of(amdComputer, intelComputer);
		//
		//		Stream<Core> coreStream = computerStream.flatMap(computer -> computer.getCores().stream());
		//
		//		coreStream.map(Core::getName)
		//			.filter(s -> s.endsWith("2"))
		//			.forEach(System.out::println);

		Stream<Core> listStream = Stream.of(amdComputer, intelComputer)
			.flatMap(computer -> computer.getCores().stream());


		Stream.of(amdComputer, intelComputer)
			.flatMap(computer -> computer.getCores().stream())
			.map(Core::getName)
			.filter(s -> s.endsWith("2"))
			.forEach(System.out::println);





	}
}
