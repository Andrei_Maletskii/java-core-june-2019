package lesson190829.stream.processor;

class Core {
	private String name;

	public Core(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
