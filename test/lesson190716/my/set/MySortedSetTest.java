package lesson190716.my.set;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.lang.reflect.Method;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class MySortedSetTest {

	private static int db = 0;
	private MySortedSet<Integer> set;

	@BeforeEach
	void setUp() {
		System.out.println("beforeEach");
		set = new MySortedSet<>();
	}

	@AfterEach
	void tearDown() {
		System.out.println("afterEach");
		set.clear();
	}

	@BeforeAll
	static void staticSetUp() {
		System.out.println("beforeAll");
		db = 1;
	}

	@AfterAll
	static void staticTearDown() {
		System.out.println("afterAll");
		db = 0;
	}

	@Test
	void shouldCreateEmptySet(TestInfo info) {
		System.out.println(info.getDisplayName());

		db = 2;

		Assertions.assertEquals(0, set.size());
	}

	@Test
	void shouldAddFirstElementToSet(TestInfo info) {
		System.out.println(info.getDisplayName());

		set.add(1);

		Assertions.assertEquals(1, set.size());
		Assertions.assertTrue(set.contains(1));
	}

	@Test
	void shouldContainSetUpElements() {
		setUpMySortedSet();

		assertContains(1, 2, 4, 5);
	}

	private void assertContains(Integer... i) {
		for (Integer integer : i) {
			Assertions.assertTrue(set.contains(integer));
		}
	}

	@Test
	void shouldAddInTheMiddleOfSet() {
		setUpMySortedSet();

		Assertions.assertTrue(set.add(3));
		Assertions.assertFalse(set.add(3));

	}

	private void setUpMySortedSet() {
		set.add(1);
		set.add(2);
		set.add(4);
		set.add(5);
	}
}