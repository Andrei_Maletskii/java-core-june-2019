package lesson190723.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FooBarTest {

	FooBar fooBar;

	@BeforeEach
	void setUp() {
		fooBar = new FooBar();
	}

	@Test
	void shouldReturnNothing() {
		String result = fooBar.fooBar(13);
		Assertions.assertEquals("nothing", result);
		Assertions.assertEquals(7, result.length());
	}

	@Test
	void shouldReturnFoo() {
		Assertions.assertEquals("foo", fooBar.fooBar(9));
		Assertions.assertEquals("foo", fooBar.fooBar(27));
		Assertions.assertEquals("foo", fooBar.fooBar(33));
	}

	@Test
	void shouldReturnBar() {
		Assertions.assertEquals("bar", fooBar.fooBar(10));
		Assertions.assertEquals("bar", fooBar.fooBar(50));
	}

	@Test
	void shouldReturnFooBar() {
		Assertions.assertEquals("foobar", fooBar.fooBar(15));
		Assertions.assertEquals("foobar", fooBar.fooBar(45));
	}

}